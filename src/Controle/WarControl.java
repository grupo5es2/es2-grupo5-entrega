package Controle;

import static Controle.WarControl.ESTADO_REMANEJAMENTO.ESCOLHE_ORIGEM;
import Interface.SpriteTerritorio;
import Interface.TelaMapa;
import Modelo.Jogo;
import Modelo.Jogador;
import Modelo.Territorio;

import Interface.TelaMenu;
import Modelo.CartaObjetivo;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import jplay.Window;

/**
 *
 */
public class WarControl {
    
    private static WarControl m_instancia = null;
    
    private Jogo m_jogo = null;
    private TelaMenu m_telaMenu = null;
    private TelaMapa m_telaMapa = null;
    
    private ESTADO_ATAQUE m_estadoAtaque = ESTADO_ATAQUE.ESCOLHE_ATACANTE;
    private ESTADO_REMANEJAMENTO m_estadoRemanejamento = ESTADO_REMANEJAMENTO.ESCOLHE_ORIGEM;
    
    public ESTADO_ATAQUE getEstadoAtaque() {
        return m_estadoAtaque;
    }
    
    public ESTADO_REMANEJAMENTO getEstadoRemanejamento() {
        return m_estadoRemanejamento;
    }
    
    public void setEstadoAtaque(ESTADO_ATAQUE estado_ataque) {
        m_estadoAtaque = estado_ataque;
    }

    public void setEstadoRemanejamento(ESTADO_REMANEJAMENTO estado_remanejamento) {
        m_estadoRemanejamento = estado_remanejamento;
    }
    
    public void setTerritorioOrigem(Territorio _territorio)
    {
        m_territorioOrigem = _territorio;        
    }
    
    private Territorio m_territorioAtaque;
    private Territorio m_territorioDefesa;
    private Territorio m_territorioOrigem;
    private int m_dadosAtaque;
    private int m_dadosDefesa;
    private boolean remanejando;
    

    private WarControl() {
    }

    public void setRemanejando(boolean booleano) {
        this.remanejando = booleano;
    }
    
    public boolean getRemanejando() {
        return this.remanejando;
    }
    
    public void setTelaMapa(TelaMapa telaMapa) {
        m_telaMapa = telaMapa;
    }
    
    public TelaMapa getTelaMapa()
    {
        return m_telaMapa;
    }

    public ArrayList<Integer> rolarDadosJogador(int id) {
        return getJogo().rolarDadosJogador(id);
    }

    public ArrayList<Integer> consolidarAtaque() {
        return getJogo().consolidarAtaque();
    }
    
    enum CompareType
    {
        NOTEQUAL,
        EQUAL
    }
    
    public enum ESTADO_ATAQUE {
        ESCOLHE_ATACANTE,
        ESCOLHE_DEFENSOR    
    }
    
    public enum ESTADO_REMANEJAMENTO {
        ESCOLHE_ORIGEM,
        ESCOLHE_DESTINO
    }
    
    /**
     *
     * @return
     */
    public static WarControl me()
    {
        if(m_instancia == null)
            m_instancia = new WarControl();
        return m_instancia;
    }
    
    /**
     *
     * @return
     */
    public Jogo getJogo()
    {
        return m_jogo;
    }
    
    /**
     *
     * @param _jogo
     */
    public void setJogo(Jogo _jogo)
    {
        m_jogo = _jogo;
    }
    
    /**
     * Obtem a primeira tela.
     * @return
     */
    public TelaMenu getTelaMenu()
    {
        return m_telaMenu;
    }
    
    /**
     * Atribui primeira tela ao controlador.
     * @param _tela
     */
    public void setTelaInicial(TelaMenu _tela)
    {
        m_telaMenu = _tela;
    }
    
    /**
     * Inicia controlador
     */
    public void start()
    {
        if(m_telaMenu != null)
        {
            m_telaMenu.start();
        }
    }   
    
    public CartaObjetivo getObjetivoHumano(){
        return m_jogo.m_objetivoHumano;
    }
    
    public Color getColorString(String strCor)
    {
        switch(strCor)
        {
            case "green" : return Color.green;
            case "gray" : return Color.gray.brighter();
            case "blue" : return Color.cyan.darker();
            case "orange" : return Color.orange.brighter();
            case "red" : return Color.red;
            case "brown" : return Color.magenta;//new Color(161,120,41);
            default : return Color.white;
        }
    }
    
    private static int getBrightness(Color c)
    {
       int r = c.getRed();
       int g = c.getGreen();
       int b = c.getBlue();
       return (int)Math.sqrt(
          r * r * .241 + 
          g * g * .691 + 
          b * b * .068);
    }

    /**
     *
     * @param janela
     * @param mapaIdTerritorioSprites
     */
    public void desenhaSpriteTerritorios(Window janela, TreeMap<Integer, SpriteTerritorio> mapaIdTerritorioSprites)
    {
        TreeMap<Integer, Jogador> jogadores = m_jogo.getMapIdJogador();
        TreeMap<Integer, Territorio> mapa = m_jogo.getMapaIdTerritorios();
        
        int id_jogador_atual = m_jogo.getJogadorAtualIndice();
        
        for(Map.Entry<Integer, Territorio> entry : mapa.entrySet()) {
            Territorio territorio = entry.getValue();
            
            String str_cor = jogadores.get(territorio.id_jogador).cor;
            Color cor_jogador = getColorString(str_cor);
            
            boolean escolhendoDefensor = m_jogo.getEstado() == Jogo.ESTADO.ATAQUE && m_estadoAtaque == ESTADO_ATAQUE.ESCOLHE_DEFENSOR;
            
            CompareType compare = CompareType.NOTEQUAL;
            if(escolhendoDefensor)
                compare = CompareType.EQUAL;
                                    
            SpriteTerritorio sprite = atualizarSpriteTerritorio(mapaIdTerritorioSprites, territorio, str_cor, compare);
            
            Boolean drawSprite = territorio.id_jogador == id_jogador_atual;
            
            if(escolhendoDefensor)
            {
                if(territorio.id_jogador != m_territorioAtaque.id_jogador && m_territorioAtaque.vizinhos.containsKey(territorio.id))
                {
                    drawSprite = true;
                }
                else
                {
                    drawSprite = false;
                }
            }
            
            if(m_territorioOrigem != null)
            {
                if(territorio.id_jogador == m_territorioOrigem.id_jogador && m_territorioOrigem.vizinhos.containsKey(territorio.id))
                {
                    drawSprite = true;
                }
                else
                {
                    drawSprite = false;
                }
            }
            
            if(drawSprite)
                sprite.draw();
            
            String label_territorio;
            if(drawSprite)
                label_territorio = territorio.nome + " (" + String.valueOf(territorio.exercitos) + ")";
            else
                label_territorio = "(" + String.valueOf(territorio.exercitos) + ")";

            Point pos_label;
            

            if(drawSprite)
            {
                int xx = (int)(territorio.pos_X + SpriteTerritorio.sprite_size + 5);
                int yy = (int)(territorio.pos_Y + Interface.TelaMenu.offsetTela() + SpriteTerritorio.sprite_size/2 + 5);
                pos_label = new Point(xx, yy);
            }
            else
            {
                int xx = (int)(territorio.pos_X + 4);
                int yy = (int)(territorio.pos_Y + Interface.TelaMenu.offsetTela() + SpriteTerritorio.sprite_size/2 + 5 );
                pos_label = new Point(xx, yy);                
            }
                        
            TelaMapa.desenhaRectTexto(janela, label_territorio, pos_label, TelaMapa.m_corLabel );
            janela.drawText(label_territorio, 
                pos_label.x,
                pos_label.y,
                cor_jogador.brighter()
                );

        }
    }

    private SpriteTerritorio getSpriteTerritorio(String str_cor, Territorio territorio) {
        String fileName = "arrows_24p/arrow_circle_" + str_cor + "_up.png";
        SpriteTerritorio sprite = new SpriteTerritorio(TelaMenu.getResource(fileName));

        sprite.height = SpriteTerritorio.sprite_size;
        sprite.width = SpriteTerritorio.sprite_size;
        sprite.x = (int) territorio.pos_X;
        sprite.y = (int) territorio.pos_Y + Interface.TelaMenu.offsetTela();
        sprite.id_jogador = territorio.id_jogador;
        sprite.id_territorio = territorio.id;
        
        return sprite;
    }

    /**
     *
     * @param _idTerritorio
     * @return
     */
    public String getNomeTerritorio(Integer _idTerritorio) {
        TreeMap<Integer, Territorio> mapa = getJogo().getMapaIdTerritorios();
        if(mapa.containsKey(_idTerritorio))
            return mapa.get(_idTerritorio).nome;
        else
            return "";
    }
    
    public Territorio getTerritorio(Integer _idTerritorio) {
        TreeMap<Integer, Territorio> mapa = getJogo().getMapaIdTerritorios();
        if(mapa.containsKey(_idTerritorio))
            return mapa.get(_idTerritorio);
        else
            return null;
    }

    private SpriteTerritorio atualizarSpriteTerritorio(TreeMap<Integer, SpriteTerritorio> _mapaIdTerSpr, Territorio _territorio, String _cor, CompareType _compareType) {
        if(!_mapaIdTerSpr.containsKey(_territorio.id))
        {
            SpriteTerritorio sprite = getSpriteTerritorio(_cor, _territorio);
            _mapaIdTerSpr.put(_territorio.id, sprite);
            return sprite;
        }
        else
        {
            SpriteTerritorio sprite = _mapaIdTerSpr.get(_territorio.id);
            Boolean compared = false;
            
            if(_compareType == CompareType.EQUAL)
                compared = sprite.id_jogador == _territorio.id_jogador;
            else if(_compareType == CompareType.NOTEQUAL)
                compared = sprite.id_jogador != _territorio.id_jogador;
            
            
            if(compared == true)
            {
                _mapaIdTerSpr.remove(_territorio.id);                
                
                _mapaIdTerSpr.put(_territorio.id,
                        getSpriteTerritorio(_cor, _territorio));
                
                return _mapaIdTerSpr.get(_territorio.id);
            }
            return sprite;
        }  
    }

    public String getNomeJogadorDeTerritorio(Integer _idTerritorio) {
        TreeMap<Integer, Territorio> mapa = getJogo().getMapaIdTerritorios();
        if(mapa.containsKey(_idTerritorio))
            return getJogo().getMapIdJogador().get(mapa.get(_idTerritorio).id_jogador).nome;
        else
            return "";
    }
    
    public String getStatus() {
        String status = "";
        switch(getJogo().getEstado())
        {
            case INICIO:
            {
                status = "Construindo...";
                break;                
            }
            
            case DISTRIBUICAO_INICIAL:
            {
                int restantes ;                
                //TODO
                restantes = getJogo().getRestanteDistribuicaoInicial(getJogo().getJogadorAtualIndice());
                if (getJogo().ehJogadorHumano()) {
                    if (restantes > 1)
                        status = "Escolha território para distribuir exército (" + String.valueOf(restantes) + " restantes)";
                    else
                        status = "Escolha território para distribuir exército (" + String.valueOf(restantes) + " restante)";
                } else {
                    if (restantes > 1)
                        status = "Escolhendo território para distribuir exército (" + String.valueOf(restantes) + " restantes)";
                    else
                        status = "Escolhendo território para distribuir exército (" + String.valueOf(restantes) + " restante)";
                }
                
                break;                
            }
            
            case DISTRIBUICAO_RODADA:
            {
                int exercitos ;                
                //TODO
                exercitos = getJogo().getRestanteDistribuicaoInicial(getJogo().getJogadorAtualIndice());
                if (getJogo().ehJogadorHumano()) {
                    if (exercitos > 1)
                        status = "Escolha territórios para distribuir (" + String.valueOf(exercitos) + " ) exércitos";
                    else
                        status = "Escolha território para distribuir (" + String.valueOf(exercitos) + " ) exército";
                } else {
                    if (exercitos > 1)
                        status = "Escolhendo territórios para distribuir (" + String.valueOf(exercitos) + " ) exércitos";
                    else
                        status = "Escolhendo território para distribuir (" + String.valueOf(exercitos) + " ) exército";
                }
                
                break;                
            }
            
            case ATAQUE:
            {
                status = "Atacando";
                break;                
            }
            
            case DISTRIBUICAO:
            {
                status = "Remanejando";
                if (m_territorioOrigem != null) {
                    status += " de " + m_territorioOrigem.nome;
                }
                break;              
            }
            
            case TROCA:
            {
                status = "Aguardando troca...";
                break;                
            }
            
            default:
            {
                status = "Erro!!!";
                break;                
            }            
        }
        
        return status;                
    }

    public String getStatusString() {
        return getStatus();
    }
    
    public String getJogadorAtualNome() {
         return getJogo().getMapIdJogador().get(getJogo().getJogadorAtualIndice()).nome;
    }
    
    public Color getJogadorAtualColor() {
        return getColorString(getJogo().getMapIdJogador().get(getJogo().getJogadorAtualIndice()).cor);
    }

    public void clicouTerritorio(Integer key, String status) {        
        switch(getJogo().getEstado())
        {
            case INICIO:
            {                
                break;                
            }
            
            case DISTRIBUICAO_INICIAL:
            {
                getJogo().distribuirEmTerritorio(key);
                getJogo().getJogadorAtual().setAcabouTurno(true);
                break;                
            }
            
            case DISTRIBUICAO_RODADA:
            {
                getJogo().distribuirEmTerritorio(key);
                break; 
            }
            
            case ATAQUE:
            {
                Jogador jogador = getJogo().getMapIdJogador().get(m_jogo.getJogadorAtualIndice());
                Territorio territorio = getJogo().getMapaIdTerritorios().get(key);                
                
                switch(m_estadoAtaque)
                {                   
                    case ESCOLHE_ATACANTE: //Deverá ser pedido o nº de atacantes (1, 2 ou 3)
                    {                                                
                        if(territorio.id_jogador == jogador.id && territorio.exercitos > 1) // && Nº de peças > 1
                        {
                            m_territorioAtaque = territorio;
                            // 1 ou 2 ou 3 dados
                            m_dadosAtaque = m_telaMapa.getTelaAtaque().pedirDados(m_territorioAtaque);
                            if(m_dadosAtaque > 0)
                            {
                                m_estadoAtaque = ESTADO_ATAQUE.ESCOLHE_DEFENSOR;
                                getJogo().atribuirAtacante(m_territorioAtaque.id, m_dadosAtaque);
                            }
                        }
                        
                        break;
                    }
                    case ESCOLHE_DEFENSOR:
                    {
                        if(territorio.id_jogador != m_territorioAtaque.id_jogador && m_territorioAtaque.vizinhos.containsKey(territorio.id))
                        {
                            m_territorioDefesa = territorio;                             
                            if(m_territorioDefesa.exercitos >= 3)
                                m_dadosDefesa = 3;
                            else if(m_territorioDefesa.exercitos == 2)
                                m_dadosDefesa = 2;
                            else
                                m_dadosDefesa = 1;
                            
                            getJogo().atribuirDefensor(m_territorioDefesa.id, m_dadosDefesa);
                            
                            m_telaMapa.getTelaAtaque().rodarAtaque(m_territorioAtaque, m_territorioDefesa, m_dadosAtaque, m_dadosDefesa);
                            
                            m_estadoAtaque = ESTADO_ATAQUE.ESCOLHE_ATACANTE;
                        }
                        break;
                    }
                }
                break;                
            }
            
            case DISTRIBUICAO: //Este caso trata do remanejamento, certo?
            {
                Jogador jogador = getJogo().getMapIdJogador().get(m_jogo.getJogadorAtualIndice());
                Territorio territorio = getJogo().getMapaIdTerritorios().get(key);
                        
                switch(m_estadoRemanejamento) {
                    
                    case ESCOLHE_ORIGEM:
                        if (jogador.id == territorio.id_jogador && territorio.exercitos > 1) {
                            m_territorioOrigem = territorio;
                            m_estadoRemanejamento = ESTADO_REMANEJAMENTO.ESCOLHE_DESTINO;
                        }
                        break;
                    case ESCOLHE_DESTINO:
                        if (m_territorioOrigem.vizinhos.containsKey(territorio.id)) {
                            getJogo().remanejar(m_territorioOrigem.id, territorio.id);
                            m_estadoRemanejamento = ESTADO_REMANEJAMENTO.ESCOLHE_ORIGEM;
                            m_territorioOrigem = null;
                        }
                        break;                        
                }
                break;                        
            }
            
            default:
            {
                break;                
            }            
        }
    }   

    public void fimTurno() {
        
        if(getJogo().ehJogadorHumano() == false)
            getJogo().rodarIA();
        
        getJogo().fimTurno();
    }

    public long getDelay() {
        if(getJogo().ehJogadorHumano() == true)
            return 0;
        return 150;
    }
    
}
