/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import jplay.GameObject;
import jplay.Window;

/**
 *
 * @author alexandre
 */
public class LabelGameObject extends GameObject {
    
    private Window win;
    private Rectangle2D m_labelBounds;
    private String m_label;
    private Color m_bgColor, m_fgColor;    
    private Font m_font;
    
    public LabelGameObject(Window parent, Rectangle2D _labelBounds, String _label, Color _bgColor, Color _fgColor)
    {
        super();
        iniciar(parent, _label, _bgColor, _fgColor);
        setLabelBounds(_labelBounds);
    }
    
    public LabelGameObject(Window parent, Rectangle2D _labelBounds, String _label, Color _bgColor)
    {
        super();
        iniciar(parent, _label, _bgColor, Color.BLACK);
        setLabelBounds(_labelBounds);
    }
    
    public LabelGameObject(Window parent, Rectangle2D _labelBounds, Color _bgColor)
    {
        super();
        iniciar(parent, "", _bgColor, Color.BLACK);
        setLabelBounds(_labelBounds);
    }  
    
    public LabelGameObject(Window parent, String label, Color _bgColor, Color _fgColor)
    {
        super();
        iniciar(parent, label, _bgColor, _fgColor);
    }
   
    public LabelGameObject(Window parent, String label, Color _bgColor)
    {
        super();
        iniciar(parent, label, _bgColor, Color.BLACK);        
    }
   
    public Font getFont() {
        return m_font;
    }

    public void setFont(Font _font) {
        m_font = _font;
        setLabelBounds(calcBounds(m_label));
    }
    
    public String getLabel() {
        return m_label;
    }

    public void setLabel(String _label) {
        this.m_label = _label;
        setLabelBounds(calcBounds(m_label));
    }

    public Color getBgColor() {
        return m_bgColor;
    }

    public void setBgColor(Color _bgColor) {
        this.m_bgColor = _bgColor;
    }

    public Color getFgColor() {
        return m_fgColor;
    }

    public void setFgColor(Color _fgColor) {
        this.m_fgColor = _fgColor;
    }

    public Rectangle2D getLabelBounds() {
        return m_labelBounds;
    }

    public void setLabelBounds(Rectangle2D _labelBounds) {
        this.m_labelBounds = _labelBounds;
        this.height = (int) getLabelBounds().getHeight();
        this.width = (int) getLabelBounds().getWidth(); 
    }
    
    private void iniciar(Window parent, String _label, Color _bgColor, Color _fgColor)
    {
        this.win = parent;
        this.m_font = this.win.getFont();
        
        setLabel(_label);
        setBgColor(_bgColor);
        setFgColor(_fgColor);
    }  
    
    private Rectangle2D calcBounds(String label)
    {
        Graphics2D g2d = (Graphics2D) win.getGameGraphics();
        
        Rectangle2D label_bounds = getFont().getStringBounds(getLabel(), g2d.getFontRenderContext());
        
        label_bounds.setRect(this.x, this.y, 
                            (int)label_bounds.getWidth(),
                            (int)label_bounds.getHeight()*1.5);
        
        return label_bounds;
    }
    
    public void draw()
    {
        Graphics2D g2d = (Graphics2D) win.getGameGraphics();

        g2d.setColor(getBgColor());

        Rectangle2D label_bounds = getLabelBounds();

        g2d.fillRoundRect((int) this.x, (int)this.y, (int)label_bounds.getWidth(), (int)label_bounds.getHeight(), 15, 15);
        win.drawText(getLabel(), (int)this.x, (int)(this.y + (label_bounds.getHeight()-(label_bounds.getHeight()/1.5)/2) + 2), getFgColor(), getFont());
    } 
}
