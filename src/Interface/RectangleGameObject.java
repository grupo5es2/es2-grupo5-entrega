/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.awt.geom.Rectangle2D;
import jplay.GameObject;

/**
 *
 * @author alexandre
 */
public class RectangleGameObject extends GameObject
{
    RectangleGameObject(Rectangle2D rect)
    {
        super();
        this.x = rect.getX();
        this.y = rect.getY();
        this.height = (int) rect.getHeight();
        this.width = (int) rect.getWidth();
    }
}
