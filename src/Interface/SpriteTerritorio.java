/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import jplay.Sprite;

/**
 *
 */
public class SpriteTerritorio extends Sprite
{

    /**
     *
     */
    public final static int sprite_size = 24;

    /**
     *
     * @param filename
     */
    public SpriteTerritorio(String filename)
    {
        super(filename);
    }

    /**
     *
     */
    public int id_jogador = 0;
    public int id_territorio = 0;
}
