/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Controle.WarControl;
import Modelo.Jogador;
import Modelo.Territorio;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import jplay.Keyboard;
import jplay.Mouse;
import jplay.Sprite;
import jplay.Window;

/**
 *
 * @author alexandre
 */
public class TelaAtaque {
    
        public static Logger logger()
    {
        return Logger.getLogger(TelaAtaque.class.getName());
    }
    
    private Window janela;
    private Mouse mouse;
    private Keyboard teclado;
    private Sprite sprite;
            
    public TelaAtaque(Window _janela) {
        iniciar(_janela);
    }
    
    private void iniciar(Window _janela)
    {
        janela  = _janela;               
        mouse   = janela.getMouse();
        teclado = janela.getKeyboard();
        
        sprite = new Sprite(TelaMenu.getResource("cartasRiskv2.jpg"), 4);
        sprite.setInitialFrame(0);
        sprite.setFinalFrame(3);
        sprite.setCurrFrame(0);
        
        sprite.x = 100; sprite.y = 100;               
    }
    
    public void executar()
    {
        boolean rodando = true;
        desenhaTela();        
        
        while(rodando)
        {
            sprite.draw();
            
            janela.update();
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;
            
            if (teclado.keyDown(Keyboard.ENTER_KEY))
                sprite.setCurrFrame((sprite.getCurrFrame() + 1) % (sprite.getFinalFrame() - sprite.getInitialFrame() + 1));
        }
    } 
         
    public void desenhaTela() {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String titulo = "Ataque";
               
        Font font = new Font("Arial", 3, 80);
        g2d.setColor(new Color(0, 0, 0, 0.80f));
        g2d.fillRoundRect(0, 0, janela.getWidth(), janela.getHeight(), 0, 0);
        janela.drawText(titulo, 380, 100, Color.white, font);
    }
    
    /**
     *
     * @param territorioAtaque
     * @return
     */
    public int pedirDados(Territorio territorioAtaque)
    {
        desenhaTela();
        
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        Font font = new Font("Arial", 0, 52);
        
        String nomeTerri = "Atacando com " + territorioAtaque.nome;
        int tamanhoNomeTerritorio = territorioAtaque.nome.length();
        if (tamanhoNomeTerritorio > 10)
            janela.drawText(nomeTerri, (int) (160 + nomeTerri.length()*1.5), 250, Color.white, font);
        if (tamanhoNomeTerritorio == 10)
            janela.drawText(nomeTerri, (int) (180 + nomeTerri.length()*1.5), 250, Color.white, font);
        else if (tamanhoNomeTerritorio >= 8 && tamanhoNomeTerritorio < 10)
            janela.drawText(nomeTerri, (int) (220 + nomeTerri.length()*1.5), 250, Color.white, font);
        else if (tamanhoNomeTerritorio == 7)
            janela.drawText(nomeTerri, (int) (235 + nomeTerri.length()*1.5), 250, Color.white, font);
        else if (tamanhoNomeTerritorio <= 6)
            janela.drawText(nomeTerri, (int) (245 + nomeTerri.length()*1.5), 250, Color.white, font);
            
        String info = "Número de dados de ataque:";
        janela.drawText(info, 210, 380, Color.white, new Font("Arial", 0, 48));
        
        boolean rodando = true;
        
        Sprite sprites[] = new Sprite[Math.min(3, territorioAtaque.exercitos - 1)];
        for(int i = 0; i < sprites.length; ++i)
        {
            sprites[i] = new Sprite(TelaMenu.getResource("dadosSprite5.png"), 12);
            sprites[i].setCurrFrame(i);
            
            int xDado;
            
            if (sprites.length == 3) {
                xDado = 410 + i*(sprites[i].width + 15);
                
            } else if (sprites.length == 2) {
                xDado = 450 + i*(sprites[i].width + 15);
            } else {
                xDado = 490 + i*(sprites[i].width + 15);
            }
            
            sprites[i].x = xDado;
            sprites[i].y = 450;
            sprites[i].draw();
        }
        
        while(rodando)
        {
            janela.update();
            
            if(mouse.isLeftButtonPressed())
            {
                for(int i = 0; i < sprites.length; ++i)
                {
                   if(mouse.isOverObject(sprites[i]))
                   {
                       return i + 1;
                   }
                }
            }
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;
        }
        return 0;
    }
    
    public void rodarAtaque(Territorio _ataque, Territorio _defesa, int _nunAtaque, int _nunDefesa)    
    {
        desenhaTela();

        Jogador jogadorA = WarControl.me().getJogo().getMapIdJogador().get(_ataque.id_jogador);
        Jogador jogadorD = WarControl.me().getJogo().getMapIdJogador().get(_defesa.id_jogador);
        
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        Font font = new Font("Arial", 0, 40);
        
        String nomeTerriA = _ataque.nome;
        janela.drawText(nomeTerriA, 200, 250, Color.white, font);
        
        String nomeTerriD = _defesa.nome;
        janela.drawText(nomeTerriD, 200 + 480, 250, Color.white, font);
        
        String info = "ROLAR";
        
        Font fonteBtn = new Font("Arial", 0, 22);
        LabelGameObject lgoA = new LabelGameObject(janela, info, WarControl.me().getColorString(jogadorA.cor));
        lgoA.x  = janela.getWidth()/2 - 40; lgoA.y = janela.getHeight() - 80;
        lgoA.setFont(fonteBtn);
                        
        lgoA.draw();
        
        LabelGameObject okBtn = new LabelGameObject(janela, "    OK    ", WarControl.me().getColorString(jogadorA.cor));        
        okBtn.x  = 0 + (okBtn.width*1.5); okBtn.y = janela.getHeight() - (okBtn.height*5);
        okBtn.setFont(fonteBtn);
        
        
        
        boolean podeRolarA = true;
        boolean podeRolarD = false;
        boolean resultado = false;
        boolean rodando = true;
       
               
        while(rodando)
        {
            janela.update();
           
            Color tc = lgoA.getBgColor();
            
            if(podeRolarA && mouse.isOverObject(lgoA))
            {                
                if(mouse.isLeftButtonPressed())
                {
                    logger().log(Level.SEVERE, "Clicou Rolar dado Ataque!");
                    
                    ArrayList<Integer> dados = WarControl.me().rolarDadosJogador(jogadorA.id);                    
                    //anicaoDados();             

                    Sprite sprites[] = new Sprite[dados.size()];
                    for(int i = 0; i < sprites.length; ++i)
                    {
                        sprites[i] = new Sprite(TelaMenu.getResource("dadosSprite5.png"), 12);
                        sprites[i].setCurrFrame( dados.get(i) - 1 );
                        sprites[i].x = 220;
                        sprites[i].y = 370 + i*(sprites[i].height + 15);
                        sprites[i].draw();
                        janela.update();
                        janela.delay(250);
                    }
                    podeRolarA = false;
                    podeRolarD = true;

                    janela.delay(500);
                }
            }
            else if(podeRolarD)
            {
                logger().log(Level.SEVERE, "Clicou Rolar dado Defesa!");

                ArrayList<Integer> dados = WarControl.me().rolarDadosJogador(jogadorD.id);                

                //anicaoDados();
                int indice_defesa = 6;
                int defesa_width = 400;

                Sprite sprites[] = new Sprite[dados.size()];
                for(int i = 0; i < sprites.length; ++i)
                {
                    sprites[i] = new Sprite(TelaMenu.getResource("dadosSprite5.png"), 12);
                    sprites[i].setCurrFrame( indice_defesa + dados.get(i) - 1 );
                    sprites[i].x = 305 + defesa_width;
                    sprites[i].y = 370 + i*(sprites[i].height + 15);
                    sprites[i].draw();
                    janela.update();
                    janela.delay(250);
                }

                podeRolarA = false;
                podeRolarD = false;
                resultado = true;    
                
                janela.delay(500);
            }
            else if(resultado)
            {
                logger().log(Level.SEVERE, "Exibindo resultado ataque!");
                ArrayList<Integer> resultadoAtaque = WarControl.me().consolidarAtaque();
                
                Sprite spriteModel = new Sprite(TelaMenu.getResource("dadosSprite5.png"), 12);
                spriteModel.setCurrFrame( 0 );
                
                Sprite sprites[] = new Sprite[resultadoAtaque.size()];
                for(int i = 0; i < sprites.length; ++i)
                {
                    if(resultadoAtaque.get(i) > 0)
                        sprites[i] = new Sprite(TelaMenu.getResource("arrows_24p/arrow_circle_green_up.png"));
                    else
                        sprites[i] = new Sprite(TelaMenu.getResource("arrows_24p/arrow_circle_red_down.png"));
                    
                    sprites[i].height = spriteModel.height;
                    sprites[i].width  = spriteModel.width;

                    sprites[i].x = 305 + 200;                    
                    sprites[i].y = 385 + i*(sprites[i].height + 15);
                    
                    sprites[i].draw();
                    janela.update();
                    janela.delay(250);
                }
                
                lgoA = okBtn;
                lgoA.x  = janela.getWidth()/2 - 40; lgoA.y = janela.getHeight() - 80;
                lgoA.draw();
                
                resultado = false;
            }
                    
            if(mouse.isLeftButtonPressed() && mouse.isOverObject(okBtn))
                rodando = false;

            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;
        }
    }
}
