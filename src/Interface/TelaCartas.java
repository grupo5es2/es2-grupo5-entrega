package Interface;

import Controle.WarControl;
import static Interface.TelaAtaque.logger;
import static Interface.TelaMenu.getResource;
import Modelo.CartaTerritorio;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import jplay.Keyboard;
import jplay.Mouse;
import jplay.Sprite;
import jplay.Window;

public class TelaCartas {
    
    private Window janela;
    private Mouse mouse;
    private Keyboard teclado;
    private Sprite tabelaTroca;
    private Sprite carta;
    private ArrayList<CartaTerritorio> listaCartas;
    private ArrayList<CartaTerritorio> listaCartasTrocas;
    private TelaTroca telaTroca;
    
    private LabelGameObject lgoTrocar;
    
    public TelaCartas(Window _janela) {
         iniciar(_janela);
    }
    
    private void iniciar(Window _janela)
    {
        janela  = _janela;               
        mouse   = janela.getMouse();
        teclado = janela.getKeyboard();       
        
        //desenhaTabelaTroca();
   }
    
     public void executar()
    {
        boolean rodando = true;
        desenhaTela();
        desenhaTabelaTroca();
        
        
        while(rodando)
        {
            desenhaCartas();
            desenhaBotaoTrocar();
            tabelaTroca.draw();
            janela.update();
            
            if(mouse.isOverObject(lgoTrocar)) //Se clicar no botão de troca...
            {
                if(mouse.isLeftButtonPressed())
                {
                    logger().log(Level.SEVERE, "Clicou TROCAR!");
                    if(possuiTroca()) //Fazer troca.
                    {
                        //telaTroca = new TelaTroca(janela, listaCartasTrocas);
                        //telaTroca.executar();
                        WarControl.me().getTelaMapa().trocaEfetuada();
                        WarControl.me().getJogo().efetuarTroca(listaCartasTrocas);
                        //desenhaCartasTroca();
                        rodando = false;
                        
                    }
                    else //Mostrar mensagem de que não pode fazer troca ainda.
                    {
                    }
                }
            }
            else
            {
            }
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;
            
            listaCartasTrocas = null;
        }
    } 
    
    public void desenhaTela() {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String titulo = "Cartas";
        String rodape = "Aperte Esc para retornar ao jogo.";
               
        Font fonteTitulo = new Font("Arial", 3, 80);
        Font fonteRodape = new Font("Arial", 0, 14);
        g2d.setColor(new Color(0, 0, 0, 0.80f));
        g2d.fillRoundRect(0, 0, janela.getWidth(), janela.getHeight(), 0, 0);
        janela.drawText(titulo, 380, 100, Color.white, fonteTitulo);
        janela.drawText(rodape, 420, janela.getHeight() - 20, Color.white, fonteRodape);
    }
    
    public void desenhaTabelaTroca(){
        tabelaTroca = new Sprite(getResource("Tabela Troca.jpg"));
        tabelaTroca.x = 0;
        tabelaTroca.y = 546;
        
        tabelaTroca.draw();
    }
    
    public void desenhaCartas() {
        //implementação do desenho das cartas do jogador na tela
        String caminho;
        String nome;
        //listaCartas = WarControl.me().getJogo().getListaCartaTerritorio(); //Teste com todas as cartas!
        listaCartas = WarControl.me().getJogo().getListaCartasJogador();

        for (int i = 0; i < listaCartas.size(); i++) {
            caminho = listaCartas.get(i).getCaminho();
            nome = listaCartas.get(i).getNome();
            carta = new Sprite(getResource(caminho + ".jpg"));
            if(i < 9){//1ª Linha
                carta.x = 100 + ((i%9)*80);
                carta.y = 120;
                janela.drawText(nome, (int)carta.x, (int)carta.y + 80, Color.WHITE, janela.getFont());
            }
            else if(i >= 9 && i < 18){//2ª Linha
                carta.x = 100 + ((i%9)*80);
                carta.y = 200;
                janela.drawText(nome, (int)carta.x, (int)carta.y + 80, Color.WHITE, janela.getFont());
            }
            else if(i >= 18 && i < 27){//3ª Linha
                carta.x = 100 + ((i%9)*80);
                carta.y = 280;
                janela.drawText(nome, (int)carta.x, (int)carta.y + 80, Color.WHITE, janela.getFont());
            }
            else if(i >= 27 && i < 36){//4ª Linha
                carta.x = 100 + ((i%9)*80);
                carta.y = 360;
                janela.drawText(nome, (int)carta.x, (int)carta.y + 80, Color.WHITE, janela.getFont());
            }
            else if(i >= 36){//5ªLinha
                carta.x = 100 + ((i%9)*80);
                carta.y = 440;
                janela.drawText(nome, (int)carta.x, (int)carta.y + 80, Color.WHITE, janela.getFont());
            }
            carta.draw();
        }
    }
    
    public void desenhaCartasTroca(){
        String caminho;
        String nome;
        
        for (int i = 0; i < listaCartasTrocas.size(); i++) {
            caminho = listaCartasTrocas.get(i).getCaminho();
            nome = listaCartasTrocas.get(i).getNome();
            carta = new Sprite(getResource(caminho + ".jpg"));
            carta.x = 100 + ((i%9)*80);
            carta.y = 120;
            janela.drawText(nome, (int)carta.x, (int)carta.y + 80, Color.WHITE, janela.getFont()); 
            
            carta.draw();
        }
    }
    
    public void desenhaBotaoTrocar(){
        String info = "TROCAR";
        
        Font fonteBtn = new Font("Arial", 0, 22);
        lgoTrocar = new LabelGameObject(janela, info, Color.white);
        lgoTrocar.x  = janela.getWidth()/2 - 40; lgoTrocar.y = janela.getHeight() - 80;
        lgoTrocar.setFont(fonteBtn);
        
        lgoTrocar.draw();
    }
    public boolean possuiTroca()
    {
        listaCartas = WarControl.me().getJogo().getListaCartasJogador();
       
        for (int i = 0; i < listaCartas.size()-1; i++) { //Confere se existem 3 iguais
            listaCartasTrocas = new ArrayList<>();
            
            CartaTerritorio cartaComparacao = listaCartas.get(i);            
            listaCartasTrocas.add(cartaComparacao);
            
            for (int j = i+1; j < listaCartas.size(); j++)
            {
                if(cartaComparacao.getSimbolo() == "?")
                {
                    cartaComparacao = listaCartas.get(j);
                    if(cartaComparacao.getSimbolo() != "?")                        
                        listaCartasTrocas.add(cartaComparacao);
                    continue;
                }
                
                CartaTerritorio cartaJ = listaCartas.get(j);
                
                if(cartaComparacao.getSimbolo() == cartaJ.getSimbolo() || cartaJ.getSimbolo() == "?" )
                    listaCartasTrocas.add(cartaJ);
                
                if(listaCartasTrocas.size() >= 3)
                    return true;
            }
        }
        
        for (int i = 0; i < listaCartas.size()-2; i++) 
        { //Confere se existem 3 diferentes entre sí
            listaCartasTrocas = new ArrayList<>();
            String s1 = listaCartas.get(i).getSimbolo();
            for (int j = i+1; j < listaCartas.size()-1; j++) 
            {
                String s2 = listaCartas.get(j).getSimbolo();
                for (int k = j+1; k < listaCartas.size(); k++) 
                {
                    String s3 = listaCartas.get(k).getSimbolo();
                    if((s1 != s2 || s1 == "?") && (s1 != s3 || s1 == "?") && s2 != s3)
                    {
                        listaCartasTrocas.add(0, listaCartas.get(i));
                        listaCartasTrocas.add(1, listaCartas.get(j));
                        listaCartasTrocas.add(2, listaCartas.get(k));
                        return true;
                    }
                }
            }
        }
        
        listaCartasTrocas = new ArrayList<>();
        return false;
    }
    
}
