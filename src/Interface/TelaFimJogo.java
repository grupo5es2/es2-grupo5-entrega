package Interface;

import Controle.WarControl;
import Modelo.Jogador;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import jplay.Keyboard;
import jplay.Window;

public class TelaFimJogo {
    
    private Window janela;
    private Keyboard teclado;
    final private Jogador vencedor;
    
    public TelaFimJogo(Window _janela, Jogador vencedor) {
        this.vencedor = vencedor;
        iniciar(_janela);
    }
    
    private void iniciar(Window _janela)
    {
        janela  = _janela;               
        teclado = janela.getKeyboard();               
   }
    
     public void executar()
    {
        boolean rodando = true;
        desenhaTela();
        
        while(rodando)
        {
            janela.update();
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY)) {
                rodando = false;
            }
        }
    } 
    
    public void desenhaTela() {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String rodape = "Aperte Esc para retornar para a tela inicial.";
        String texto;
        String complemento = ""; //mostrara o nome do vencedor em caso de derrota
        
        if (vencedor.nome.equals("Humano")) {
            texto = "Vitória!";
        } else {
            texto = "Derrota...";
            complemento = "Vencedor: " + vencedor.nome; //algum bot vencedor
        }
               
        Font fonteTitulo = new Font("Arial", 3, 80);
        Font fonteRodape = new Font("Arial", 0, 14);
        g2d.setColor(new Color(0, 0, 0, 0.80f));
        g2d.fillRoundRect(0, 0, janela.getWidth(), janela.getHeight(), 0, 0);
        janela.drawText(texto, 380, (janela.getHeight()/2) + 20 , Color.white, fonteTitulo);
        janela.drawText(rodape, 400, janela.getHeight() - 20, Color.white, fonteRodape);
        janela.drawText(complemento, 470, janela.getHeight()/2 + 60, Color.white, fonteRodape);
    }
}
