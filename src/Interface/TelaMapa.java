/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Controle.WarControl;
import Modelo.CartaTerritorio;
import Modelo.Jogo;
import Modelo.Jogo.ESTADO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import jplay.GameImage;
import jplay.Keyboard;
import jplay.Mouse;
import jplay.Window;

/**
 *
 */
public class TelaMapa {
    
    public static Logger logger()
    {
        return Logger.getLogger(TelaMapa.class.getName());
    }

    private Window janela;
    private Mouse mouse;
    private Keyboard teclado;
    private GameImage telaMapa;
    private TelaAtaque telaAtaque;
    private TelaObjetivo telaObjetivo;
    private TelaCartas telaCartas;
    
    private String label_superior;
    private String label_inferior;
    private String label_troca;
    
    private int qtdTrocasMesa = 0; //Para somar +1, utilizar trocaEfetuada()
    
    private TreeMap<Integer, SpriteTerritorio> spritesTerritorios;
    
    private static int m_offset;
    
    public static final Color m_corLabel = new Color(0, 0, 0, (int)(0.75 * 255));

    /**
     *
     * @param _janela
     */
    public TelaMapa(Window _janela) {
        iniciar(_janela);
    }
    
    public TelaAtaque getTelaAtaque()
    {
        return telaAtaque;
    }
    
    private void iniciar(Window _janela)
    {
        janela  = _janela;
        
        label_superior = "";
        label_inferior = "";
        label_troca = "";
        
        mouse   = janela.getMouse();
        teclado = janela.getKeyboard();        
        
        telaMapa = new GameImage(TelaMenu.getResource(WarControl.me().getJogo().getMapaFileName()));
        telaCartas = new TelaCartas(janela);
        telaObjetivo = new TelaObjetivo(janela);
        telaAtaque = new TelaAtaque(janela);
        
        telaMapa.x = (janela.getWidth() - telaMapa.width) / 2;
        telaMapa.y = (janela.getHeight() - telaMapa.height) / 2;
        
        m_offset = (int)telaMapa.y;
        
        spritesTerritorios = new TreeMap<>();
        
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        
    }
    
    static int offset() {
        return m_offset;
    }

    
    /**
     * Loop principal
     */
    public void executar()
    {
        boolean rodando = true;
        
        WarControl.me().setTelaMapa(this);
        
        
        while(rodando){
            janela.clear(Color.BLACK);
            
            telaMapa.draw();
            
            label_superior = "";
            label_inferior = "";
            label_troca = "";
            
            boolean btPress = mouse.isLeftButtonPressed();
            
            RectangleGameObject labelJogador = desenhaLabelJogadorAtual();
            RectangleGameObject labelObjetivo = desenhaLabelObjetivo();
            RectangleGameObject labelCartas = desenhaLabelCartas();
            RectangleGameObject labelPassarVez = null;
            RectangleGameObject labelRemanejar = null;
            
            if (WarControl.me().getJogo().ehJogadorHumano()) {
                
                if (WarControl.me().getJogo().getEstado() != ESTADO.DISTRIBUICAO_INICIAL && 
                    WarControl.me().getJogo().getEstado() != ESTADO.DISTRIBUICAO_RODADA) 
                {
                    labelPassarVez = desenhaLabelPassarVez();

                    if (!WarControl.me().getRemanejando()) {
                        labelRemanejar = desenhaLabelRemanejar();
                    }
                }
            }
            
            WarControl.me().desenhaSpriteTerritorios(janela, spritesTerritorios);
            
            //Percorre a lista de icones de territorio...
            for(Map.Entry<Integer, SpriteTerritorio> entry : spritesTerritorios.entrySet())
            {
                SpriteTerritorio spr_territorio = entry.getValue();
                if(mouse.isOverObject(spr_territorio))
                {
                    String nome_terri = WarControl.me().getNomeTerritorio(entry.getKey());
                    String nome_jogador = WarControl.me().getNomeJogadorDeTerritorio(entry.getKey());

                    if (nome_jogador.equals("Humano"))
                        label_inferior = "Sobre territorio " + nome_terri + " - Pertence a você.";
                    else
                        label_inferior = "Sobre territorio " + nome_terri + " - Pertence a " + nome_jogador + ".";

                    if(btPress && WarControl.me().getJogo().ehJogadorHumano())
                        WarControl.me().clicouTerritorio(entry.getKey(), "");

                    break;
                }
            }
            
            if(label_superior.isEmpty())
                label_superior = WarControl.me().getStatusString();
            label_troca = "Nº de trocas efetuadas: ";
            
            janela.drawText(label_inferior, 5, janela.getHeight() - 20, Color.yellow);
            janela.drawText(label_superior, 5, 25 , WarControl.me().getJogadorAtualColor());
            janela.drawText(label_troca + qtdTrocasMesa, 715, 26 , Color.YELLOW);
            
            //Verifica se clicou em alguma coisa
            if(btPress)
            {
                if(mouse.isOverObject(labelJogador))
                {
                    logger().log(Level.SEVERE, "clickLabelJogador");
                }
                
                if(mouse.isOverObject(labelObjetivo))
                {
                    logger().log(Level.SEVERE, "clickLabelObjetivo");
                    telaObjetivo.executar();//se mouse estiver em cima do botao objetivo e clicar, chama a telaObjetivo
                }

                if(mouse.isOverObject(labelCartas))
                {
                    logger().log(Level.SEVERE, "clickLabelCartas");
                    telaCartas.executar();//se mouse estiver em cima do botao objetivo e clicar, chama a telaObjetivo
                }
                Jogo.ESTADO estado = WarControl.me().getJogo().getEstado();
                if (estado != ESTADO.DISTRIBUICAO_INICIAL &&
                    estado != ESTADO.DISTRIBUICAO_RODADA  && 
                    WarControl.me().getJogo().ehJogadorHumano()) {
                    
                    if (!WarControl.me().getRemanejando()) {
                        if (mouse.isOverObject(labelRemanejar)) //Termina ataque e vai remanejar...
                        {
                            WarControl.me().setRemanejando(true);
                            WarControl.me().getJogo().setEstado(ESTADO.DISTRIBUICAO);
                            janela.update();
                            logger().log(Level.SEVERE, "clickLabelRemanejar");

                        }
                    }

                    if (mouse.isOverObject(labelPassarVez)) //Confere se alcançou o objetivo, senão passa a vez.
                    {
                        logger().log(Level.SEVERE, "clickLabelPassarVez");
                        //Confere se o jogador cumpriu o objetivo.
                       // if (confereObjetivo()) //Se quiser visualizar, troque essa condição por true mas não esqueça de voltar ao original!
                       // {
                            //JOGADOR GANHOU! ACABAR COM JOGO!!!!!!J
                            //TelaFimJogo telaFim = new TelaFimJogo(janela, WarControl.me().getJogo().getJogadorAtual());
                            //telaFim.executar();
                        //} //Senão, encerra o turno...
                        //else {
                            WarControl.me().setRemanejando(false);
                            WarControl.me().getJogo().getJogadorAtual().setAcabouTurno(true);

                            //Próximo jogador recebe o turno.
                       // }
                    }
                }
            }
            
            janela.update();
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
            {
                if(WarControl.me().getEstadoAtaque() == WarControl.ESTADO_ATAQUE.ESCOLHE_DEFENSOR)
                {
                    WarControl.me().setEstadoAtaque(WarControl.ESTADO_ATAQUE.ESCOLHE_ATACANTE);
                }
                else if(WarControl.me().getEstadoRemanejamento() == WarControl.ESTADO_REMANEJAMENTO.ESCOLHE_DESTINO)
                {
                    WarControl.me().setEstadoRemanejamento(WarControl.ESTADO_REMANEJAMENTO.ESCOLHE_ORIGEM);
                    WarControl.me().setTerritorioOrigem(null);
                }
                else
                {
                    TelaSair ts = new TelaSair(janela);
                    ts.executar();
                    if(ts.getConfirma())
                        rodando = false;
                }
            }
            
            janela.delay(WarControl.me().getDelay());
            
            WarControl.me().fimTurno();
        }
    }
    
    public static boolean confereObjetivo()
    {
        return WarControl.me().getObjetivoHumano().confereObjetivo();
    }
    
    public void trocaEfetuada()
    {
        qtdTrocasMesa++;
    }
    
    public int getNumTrocas()
    {
        return qtdTrocasMesa;
    }   
    
    public static Rectangle2D getLabelBounds(Window win, String label)
    {
        Graphics2D g2d = (Graphics2D) win.getGameGraphics();
        Rectangle2D label_bounds = win.getFont().getStringBounds(label, g2d.getFontRenderContext());
        return label_bounds;
    }
    
    public static void desenhaRectTexto(Window win, String label, Point pos, Color c)
    {
        Graphics2D g2d = (Graphics2D) win.getGameGraphics();
        Rectangle2D label_bounds = getLabelBounds(win, label);
        
        g2d.setColor(c);
        g2d.fillRoundRect(pos.x - 2, (int)(pos.y - label_bounds.getHeight()) + 1, (int)(label_bounds.getWidth() + 5), (int)(label_bounds.getHeight() + 5 ), 10, 10);        
    }
    
    public static RectangleGameObject desenhaLabel(Window win, String label, Color c, int pos_x, int pos_y)
    {
            Graphics2D g2d = (Graphics2D) win.getGameGraphics();            
            g2d.setColor(c);
            
            Rectangle2D label_bounds = win.getFont().getStringBounds(label, g2d.getFontRenderContext());
            label_bounds.setRect(pos_x - 1, (int)(pos_y - label_bounds.getHeight()), (int)(label_bounds.getWidth() + 7), (int)(label_bounds.getHeight()*2));
            
            g2d.fillRoundRect((int)label_bounds.getX(), (int)label_bounds.getY(), (int)label_bounds.getWidth(), (int)label_bounds.getHeight(), 15, 15);
            win.drawText(label, pos_x + 1, pos_y + 4, Color.BLACK);
            
            return new RectangleGameObject(label_bounds);
    }
    
     /** Desenha um retângulo com a cor do jogador e o nome em preto,.
     *   no canto superior direito
     */   
    private RectangleGameObject desenhaLabelJogadorAtual()
    {
            Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
            String label_jogador_atual = "Jogador Atual: " + WarControl.me().getJogadorAtualNome();            
            g2d.setColor(WarControl.me().getJogadorAtualColor());
            
            Rectangle2D label_bounds = janela.getFont().getStringBounds(label_jogador_atual, g2d.getFontRenderContext());
            int pos_x = (int) (janela.getWidth() - label_bounds.getWidth()) - 20;
            int pos_y = 22;
            
            label_bounds.setRect(pos_x - 1, (int)(pos_y - label_bounds.getHeight()), (int)(label_bounds.getWidth() + 7), (int)(label_bounds.getHeight()*2));
            
            g2d.fillRoundRect((int)label_bounds.getX(), (int)label_bounds.getY(), (int)label_bounds.getWidth(), (int)label_bounds.getHeight(), 15, 15);
            janela.drawText(label_jogador_atual, pos_x + 1, pos_y + 4, Color.BLACK);
            
            return new RectangleGameObject(label_bounds);
    }
    
    /**
     * Desenha um retângulo para o botão que será para consultar seu objetivo
     */
    private RectangleGameObject desenhaLabelObjetivo()
    {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String label_objetivo = "Visualizar objetivo";        
        g2d.setColor(Color.orange);
         
        Rectangle2D label_bounds = janela.getFont().getStringBounds(label_objetivo, g2d.getFontRenderContext());
        int pos_x = (int) (janela.getWidth() - label_bounds.getWidth()) - 20;
        int pos_y = janela.getHeight() - 22;
        
        label_bounds.setRect(pos_x - 1, (int)(pos_y - label_bounds.getHeight()), (int)(label_bounds.getWidth() + 7), (int)(label_bounds.getHeight()*2));
        
        g2d.fillRoundRect((int)label_bounds.getX(), (int)label_bounds.getY(), (int)label_bounds.getWidth(), (int)label_bounds.getHeight(), 15, 15);
        janela.drawText(label_objetivo, pos_x + 2, janela.getHeight() - 18, Color.BLACK);
        
        return new RectangleGameObject(label_bounds);
    }
       
    /**
     * Desenha um retângulo para o botão que será para consultar as cartas
     */
    private RectangleGameObject desenhaLabelCartas()
    {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String label_cartas = "Visualizar cartas";
        g2d.setColor(Color.orange);
        
        Rectangle2D label_bounds = janela.getFont().getStringBounds(label_cartas, g2d.getFontRenderContext());
        int pos_x = (int) (janela.getWidth() - label_bounds.getWidth()) - 140;
        int pos_y = janela.getHeight() - 22;
        
        label_bounds.setRect(pos_x - 1, (int)(pos_y - label_bounds.getHeight()), (int)(label_bounds.getWidth() + 7), (int)(label_bounds.getHeight()*2));
            
        g2d.fillRoundRect((int)label_bounds.getX(), (int)label_bounds.getY(), (int)label_bounds.getWidth(), (int)label_bounds.getHeight(), 15, 15);
        janela.drawText(label_cartas, pos_x + 2, janela.getHeight() - 18, Color.BLACK);
        
        return new RectangleGameObject(label_bounds);
    }
    
    /**
     * Desenha um retângulo para o botão que será para encerrar o turno
     */
    private RectangleGameObject desenhaLabelPassarVez()
    {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String label_passarVez = "Passar a vez";
        g2d.setColor(Color.orange);
        
        Rectangle2D label_bounds = janela.getFont().getStringBounds(label_passarVez, g2d.getFontRenderContext());
        int pos_x = (int) (janela.getWidth() - label_bounds.getWidth()) - 252;
        int pos_y = janela.getHeight() - 22;
        
        label_bounds.setRect(pos_x - 1, (int)(pos_y - label_bounds.getHeight()), (int)(label_bounds.getWidth() + 7), (int)(label_bounds.getHeight()*2));
            
        g2d.fillRoundRect((int)label_bounds.getX(), (int)label_bounds.getY(), (int)label_bounds.getWidth(), (int)label_bounds.getHeight(), 15, 15);
        janela.drawText(label_passarVez, pos_x + 2, janela.getHeight() - 18, Color.BLACK);
        
        return new RectangleGameObject(label_bounds);
    }
    
    /**
     * Desenha um retângulo para o botão que será para encerrar o turno
     */
    private RectangleGameObject desenhaLabelRemanejar()
    {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String label_remanejar = "Remanejar";
        g2d.setColor(Color.orange);
        
        Rectangle2D label_bounds = janela.getFont().getStringBounds(label_remanejar, g2d.getFontRenderContext());
        int pos_x = (int) (janela.getWidth() - label_bounds.getWidth()) - 345;
        int pos_y = janela.getHeight() - 22;
        
        label_bounds.setRect(pos_x - 1, (int)(pos_y - label_bounds.getHeight()), (int)(label_bounds.getWidth() + 7), (int)(label_bounds.getHeight()*2));
            
        g2d.fillRoundRect((int)label_bounds.getX(), (int)label_bounds.getY(), (int)label_bounds.getWidth(), (int)label_bounds.getHeight(), 15, 15);
        janela.drawText(label_remanejar, pos_x + 2, janela.getHeight() - 18, Color.BLACK);
        
        return new RectangleGameObject(label_bounds);
    }
    
    public Window getJanela() {
        return this.janela;
    }
}