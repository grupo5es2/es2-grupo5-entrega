package Interface;

import Controle.WarControl;

import java.awt.Color;

import java.util.logging.Level;
import java.util.logging.Logger;

import jplay.GameImage;
import jplay.Keyboard;
import jplay.Window;
import jplay.Sprite;
import jplay.Mouse;

/**
 *

 */
public class TelaMenu {

    public static int desenhaEscolhaDados(int exercitos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Window janela;
    private Mouse mouse;
    private Keyboard teclado;
    
    private GameImage telaMenu;
    
    private Sprite    spriteNumJogadores;
    private int       numJogadores;
    
    public static Logger logger()
    {
        return Logger.getLogger(TelaMenu.class.getName());
    }
    
    public static int offsetTela() {
        return TelaMapa.offset();
    }
            
   
    int tela;

    /**
     *
     */
    public static final int TELA_MENU = 0;

    /**
     *
     */
    public static final int TELA_MAPA = 1;
     
    /**
     *
     */
    public TelaMenu(){        
    }
    
    /**
     *
     */
    public void start()
    {
        iniciar();
        executar();
        encerrar();
    }
     
    /**
     *
     */
    public void iniciar() {
        janela = new Window(1024, 768);
        janela.setBackground(Color.GRAY);   
        janela.setCursor(janela.createCustomCursor(getResource("CR_Cursor.png")));
                       
        mouse = janela.getMouse();             
        teclado = janela.getKeyboard();
        
        telaMenu = new GameImage(getResource("tela_menu.jpg"));
        
        numJogadores = 2;
        ciclaspriteNumJogadores();
        
        tela = TELA_MENU;
    }

    /**
     *
     */
    public void executar(){
        boolean rodando = true;
        
        while(rodando){
            janela.clear(Color.BLACK);
            
            if(tela == TELA_MENU){
                telaMenu.draw();                
                spriteNumJogadores.draw();
                
                //verifica se clicou...
                if(mouse.isLeftButtonPressed())
                {
                    //No numero de jogadores
                    if(mouse.isOverObject(spriteNumJogadores))
                        ciclaspriteNumJogadores(); //Circula entre (3,6)
                    
                    //Na palavra 'iniciar'
                    else if(mouse.isOverArea(356,367,615,419))
                    {
                        try {
                            WarControl.me().getJogo().iniciaJogo(numJogadores);
                        } catch (Exception ex) {
                            Logger.getLogger(Modelo.Jogo.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        tela = TELA_MAPA;
                    }
                }
            }
            else if(tela == TELA_MAPA){
                TelaMapa telaMapa = new TelaMapa(janela);
                telaMapa.executar();
                tela = TELA_MENU;
            }            
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;

            janela.update();
        }
    }
    
    private void ciclaspriteNumJogadores() {
        numJogadores++;
        if (numJogadores > 6)
            numJogadores = 3;
        
        logger().log(Level.SEVERE, String.format("Numero de jogadores: %1$d", numJogadores ), "");
        
        switch(numJogadores)
        {
            case 3: 
                spriteNumJogadores = new Sprite(getResource("jogadores3.png"));
                break;
            case 4:
                spriteNumJogadores = new Sprite(getResource("jogadores4.png"));
                break;
            case 5:
                spriteNumJogadores = new Sprite(getResource("jogadores5.png"));
                break;
            case 6:
                spriteNumJogadores = new Sprite(getResource("jogadores6.png"));
                break;
            default:                
        }
        
        spriteNumJogadores.setX(660);
        spriteNumJogadores.setY(295);
    }

    private void encerrar() {
        mouse = null;
        teclado = null;
        
        //Sai do jogo
        janela.exit();
    }

    /**
     *
     * @param _fileName
     * @return
     */
    public static String getResource(String _fileName)
    {
        return System.getProperty("user.dir") + "\\resource\\" + _fileName;
    }
}
