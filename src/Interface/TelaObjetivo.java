package Interface;

import static Interface.TelaMenu.getResource;

import Controle.WarControl;

import Modelo.CartaObjetivo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import jplay.Keyboard;
import jplay.Mouse;
import jplay.Sprite;
import jplay.Window;

public class TelaObjetivo {
    
    private Window janela;
    private Mouse mouse;
    private Keyboard teclado;
    CartaObjetivo objetivo;
    Sprite carta;
    String descricao;
    
    public TelaObjetivo(Window _janela) {
        iniciar(_janela);
    }
    
    private void iniciar(Window _janela)
    {
        janela  = _janela;               
        mouse   = janela.getMouse();
        teclado = janela.getKeyboard();        
   }
    
     public void executar()
    {
        boolean rodando = true;
        desenhaTela();
        desenhaCartaObjetivo();
        
        while(rodando)
        {
            janela.update();
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;
        }
    } 
     
    public void desenhaTela() {
        
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String titulo = "Objetivo";
        String rodape = "Aperte Esc para retornar ao jogo.";
        
        Font fonteTitulo = new Font("Arial", 3, 80);
        Font fonteRodape = new Font("Arial", 0, 14);
        g2d.setColor(new Color(0, 0, 0, 0.80f));
        g2d.fillRoundRect(0, 0, janela.getWidth(), janela.getHeight(), 0, 0);
        janela.drawText(titulo, 360, 100, Color.white, fonteTitulo);   
        janela.drawText(rodape, 420, janela.getHeight() - 20, Color.white, fonteRodape);
    }
    
    public void desenhaCartaObjetivo(){
        objetivo = WarControl.me().getObjetivoHumano();
        
        carta = new Sprite(getResource(objetivo.getCaminho() + ".jpg"));
        carta.x = (janela.getWidth()/2)-73;
        carta.y = (janela.getHeight()/2)-113;
        carta.draw();
        //Escreve descrição da carta, mais ou menos centralizada de acordo com o sprite carta.
        descricao = objetivo.getDesc();
        janela.drawText(descricao, (int)(carta.x - (descricao.length())), (int)carta.y + 250, Color.WHITE, janela.getFont());
    }
}
