package Interface;

import Modelo.Jogador;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import jplay.Keyboard;
import jplay.Mouse;
import jplay.Window;

public class TelaSair {
    
    private Window janela;
    private Keyboard teclado;
    private Mouse mouse;    
    
    private boolean confirma = false;
    
    public boolean  getConfirma()
    {
        return confirma;
    }
    
    public TelaSair(Window _janela) {        
        iniciar(_janela);
    }
    
    private void iniciar(Window _janela)
    {
        janela  = _janela;               
        teclado = janela.getKeyboard();           
        mouse = janela.getMouse();
   }
    
     public void executar()
    {
        boolean rodando = true;
        desenhaTela();
        
        Font fonteBtn = new Font("Arial", 0, 22);
        
        LabelGameObject simBtn = new LabelGameObject(janela, "    SIM    ", Color.WHITE);        
        simBtn.setFont(fonteBtn);
        simBtn.x  = (janela.getWidth()/2) - simBtn.width - 15; simBtn.y = (janela.getHeight()/2);
        
        LabelGameObject naoBtn = new LabelGameObject(janela, "    NÃO    ", Color.WHITE);
        naoBtn.setFont(fonteBtn);
        naoBtn.x  = (janela.getWidth()/2) + 15; naoBtn.y = simBtn.y;
        
                
        simBtn.draw();
        naoBtn.draw();
        
        while(rodando)
        {
            janela.update();
            
            if(mouse.isLeftButtonPressed())
            {
                if(mouse.isOverObject(naoBtn))
                {
                    this.confirma = false;
                    rodando = false;
                }
                else if(mouse.isOverObject(simBtn))
                {                   
                    this.confirma = true;
                    rodando = false;
                }
            }
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;
        }
    } 
    
    public void desenhaTela() {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String rodape = "Aperte Esc para retornar para a tela inicial.";
        String texto;
        String complemento; //mostrara o nome do vencedor em caso de derrota
        
        texto = "Deseja realmente sair do jogo?";
               
        Font fonteTitulo = new Font("Arial", 3, 52);
        Font fonteRodape = new Font("Arial", 0, 14);
        
        g2d.setColor(new Color(0, 0, 0, 0.80f));
        g2d.fillRoundRect(0, 0, janela.getWidth(), janela.getHeight(), 0, 0);
        
        Rectangle2D label_bounds = fonteTitulo.getStringBounds(texto, g2d.getFontRenderContext());
        janela.drawText(texto, (int)((janela.getWidth()/2) - (label_bounds.getWidth()/2)), (janela.getHeight()/2) - 100 , Color.white, fonteTitulo);
        janela.drawText(rodape, 400, janela.getHeight() - 20, Color.white, fonteRodape);
    }
       
    public void desenhaCartas() {
        //implementação do desenho das cartas do jogador na tela
    }
}
