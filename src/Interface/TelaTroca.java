/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Interface;

import Controle.WarControl;
import jplay.Keyboard;
import jplay.Mouse;
import jplay.Sprite;
import jplay.Window;
import static Interface.TelaCartas.*;
import static Interface.TelaMenu.getResource;
import Modelo.CartaTerritorio;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class TelaTroca {
    private Window janela;
    private Mouse mouse;
    private Keyboard teclado;
    private Sprite tabelaTroca;
    
    private Sprite carta;
    private ArrayList<CartaTerritorio> listaCartasTrocas;
    private ArrayList<Sprite> spritesCartasTrocas;
    
    private LabelGameObject lgoTrocar;
    
    public TelaTroca(Window _janela, ArrayList<CartaTerritorio> trocas){
        iniciar(_janela, trocas);
    }
    
    private void iniciar(Window _janela, ArrayList<CartaTerritorio> trocas)
    {
        janela  = _janela;               
        mouse   = janela.getMouse();
        teclado = janela.getKeyboard();       
        
        listaCartasTrocas = trocas;
        
        spritesCartasTrocas = new ArrayList<>();
   }
    
    public void executar()
    {
        boolean rodando = true;
        desenhaTela();
        desenhaTabelaTroca();
        desenhaCartasTroca();
        
        while(rodando)
        {            
            janela.update();
            if(mouse.isOverObject(lgoTrocar)) //Se clicar no botão de troca...
            {
                if(mouse.isLeftButtonPressed())
                {
                    WarControl.me().getTelaMapa().trocaEfetuada();
                    WarControl.me().getJogo().efetuarTroca(listaCartasTrocas);                    
                    rodando = false;
                }
            }
            
            if(mouse.isLeftButtonPressed())
            {
                for(Sprite carta : spritesCartasTrocas)
                {
                    if(mouse.isOverObject(carta))
                    {
                        
                    }
                }
            }
            
            if (teclado.keyDown(Keyboard.ESCAPE_KEY))
                rodando = false;
        }
        
    }
    
    public int efetuaTroca(int qtd)
    {
        
        return 0;
    }
    
    public void desenhaTabelaTroca(){
        tabelaTroca = new Sprite(getResource("Tabela Troca.jpg"));
        tabelaTroca.x = 0;
        tabelaTroca.y = 546;
        
        tabelaTroca.draw();
    }
    
    public void desenhaTela() {
        Graphics2D g2d = (Graphics2D) janela.getGameGraphics();
        String titulo = "Troca";
        String rodape = "Aperte Esc para retornar ao jogo.";
               
        Font fonteTitulo = new Font("Arial", 3, 80);
        Font fonteRodape = new Font("Arial", 0, 14);
        g2d.setColor(new Color(0, 0, 0, 0.80f));
        g2d.fillRoundRect(0, 0, janela.getWidth(), janela.getHeight(), 0, 0);
        janela.drawText(titulo, 380, 100, Color.white, fonteTitulo);
        janela.drawText(rodape, 420, janela.getHeight() - 20, Color.white, fonteRodape);
    }
    
    public void desenhaCartasTroca(){
        String caminho;
        String nome;
        
        for (int i = 0; i < listaCartasTrocas.size(); i++) {
            caminho = listaCartasTrocas.get(i).getCaminho();
            nome = listaCartasTrocas.get(i).getNome();
            carta = new Sprite(getResource(caminho + ".jpg"));
            carta.x = 100 + (i*80);
            carta.y = 120;
            
            spritesCartasTrocas.add(carta);
                    
            janela.drawText(nome, (int)carta.x, (int)carta.y + 80, Color.WHITE, janela.getFont()); 
            
            carta.draw();
        }
    }
    
    public void desenhaBotaoTrocar(){
        String info = "TROCAR";
        
        Font fonteBtn = new Font("Arial", 0, 22);
        lgoTrocar = new LabelGameObject(janela, info, Color.white);
        lgoTrocar.x  = janela.getWidth()/2 - 40; lgoTrocar.y = janela.getHeight() - 80;
        lgoTrocar.setFont(fonteBtn);
        
        lgoTrocar.draw();
    }
    
}
