package Modelo;

/**
 *
 * @author Felipe Moreira
 */
public interface Baralho {
    
    public Baralho embaralhar();
    
    public Carta pegarCarta();
}
