package Modelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementação de jogador controlado por IA
 */
public class Bot extends Jogador {

    TreeMap<Integer, Continente> m_continentes = new TreeMap<>();
    TreeMap<Integer, TreeMap<Integer, Territorio>> m_idContinentesTerritorios = new TreeMap<>();
    TreeMap<Integer, Integer> m_territoriosPorContinente = new TreeMap<>();

    Comparator< Continente> m_continenteComparator = 
        new Comparator< Continente>() {
            @Override
            public int compare(Continente o1, Continente o2) {
                
               if(!m_territoriosPorContinente.containsKey(o1.id))
                   return 0;

               if(!m_territoriosPorContinente.containsKey(o2.id))
                   return 0;

               int diff1 = o1.territorios.size() - m_territoriosPorContinente.get(o1.id);
               int diff2 = o2.territorios.size() - m_territoriosPorContinente.get(o2.id);

               return diff1 - diff2;
            }
        };
    
    Comparator< Continente> m_continenteComparatorSize = 
        new Comparator< Continente>() {
            @Override
            public int compare(Continente o1, Continente o2) {
                
               if(!m_territoriosPorContinente.containsKey(o1.id))
                   return 0;

               if(!m_territoriosPorContinente.containsKey(o2.id))
                   return 0;

               int diff1 = o1.territorios.size();
               int diff2 = o2.territorios.size();

               return diff1 - diff2;
            }
        };
    
    Comparator< Continente> m_continenteComparatorOcupados = 
        new Comparator< Continente>() {
            @Override
            public int compare(Continente o1, Continente o2) {
                
               if(!m_territoriosPorContinente.containsKey(o1.id))
                   return 0;

               if(!m_territoriosPorContinente.containsKey(o2.id))
                   return 0;

               int diff1 = m_territoriosPorContinente.get(o1.id);
               int diff2 = m_territoriosPorContinente.get(o2.id);

               return diff1 - diff2;
            }
        };
    
    Comparator< Territorio> m_territorioComparator = 
        new Comparator< Territorio>() {
            @Override
            public int compare(Territorio o1, Territorio o2) {

                int diff1 = o1.exercitos - o2.exercitos;
                int diff2 = o1.vizinhos.size() - o2.vizinhos.size();

                return diff1 - diff2;
            }
        };
    
     Comparator< Territorio> m_vizinhosComparator = 
        new Comparator< Territorio>() {
            public int m_parametro;
            @Override
            public int compare(Territorio o1, Territorio o2) {
                if(o1.id_jogador == m_parametro)
                    return -1;
                
                if(o2.id_jogador == m_parametro)
                    return 1;
                
                int diff1 = o1.exercitos - o2.exercitos;
                int diff2 = o1.vizinhos.size() - o2.vizinhos.size();

                return diff1 - diff2;
            }
        };

    public static Logger logger() {
        return Logger.getLogger(Bot.class.getName());
    }

    /**
     * Contrutor padrão, nome padrão = "Bot N"
     *
     * @param id_jogador
     */
    public Bot(int id_jogador) {
        super(id_jogador);
        nome = "Bot " + String.valueOf(id_jogador - 1);
    }

    @Override
    boolean ehJogadorHumano() {
        return false;
    }

    void acao(Jogo.ESTADO _estado, Jogo jogo) {

        atualizar(jogo);

        switch (_estado) {
            case DISTRIBUICAO_INICIAL:
            {
                jogo.distribuirEmTerritorio(escolherDistribuicaoInicial(jogo));
                m_acabarTurno = true;
                break;
            }
            
            case DISTRIBUICAO_RODADA:
            {
                int bonus = jogo.getDistribuicaoRodada(id);
                for(int i = 0; i < bonus; ++i)
                {
                    int idTerritorio = escolherDistribuicaoInicial(jogo);                       
                    jogo.distribuirEmTerritorio(idTerritorio);

                    logger().log(Level.SEVERE, String.format("Jogador %1$s incrementou territorio: %2$s restante: %3$d", this.nome, jogo.getMapaIdTerritorios().get(idTerritorio).nome, bonus-i), "");
                }
                
                break;
            }
            
            case ATAQUE:
            {
                atacar(jogo);
                m_acabarTurno = true;
                break;                
            } 
        }
    }
    
    @Override
    public void atacar(Jogo jogo) {
        Collection<Continente> continentes = m_continentes.values();
        List<Continente> list = new ArrayList<>(continentes);
        Collections.sort(list, m_continenteComparatorSize);
        Collections.sort(list, m_continenteComparatorOcupados);

        Collection<Territorio> territorios = m_idContinentesTerritorios.get(list.get(0).id).values();
        List< Territorio> list2 = new ArrayList<>(territorios);
        Collections.sort(list2, m_territorioComparator);
        
        int diff = 0;
        Iterator<Territorio> itT = list2.iterator();
        while (itT.hasNext()) {
            Territorio next = itT.next();
            
            if(next.exercitos == 1)
                continue;
            
            Collection<Territorio> vizinhos = next.vizinhos.values();
            List< Territorio> list3 = new ArrayList<>(vizinhos);
            Collections.sort(list3, m_vizinhosComparator);
            
            Iterator<Territorio> itV = list3.iterator();
            while(itV.hasNext())
            {
                Territorio vizinho = itV.next();
                if(vizinho.id_jogador == this.id)
                    continue;
                
                diff = next.exercitos - vizinho.exercitos;
                
                if(diff >= 3)
                {
                    jogo.realizarAtaque(next.id, vizinho.id, 3);
                    return;
                }
                else if(diff > 0)
                {
                    jogo.realizarAtaque(next.id, vizinho.id, diff);
                    return;
                }
            }            
        }
    }

    private void atualizar(Jogo jogo) {
        m_continentes.clear();
        m_territoriosPorContinente.clear();

        TreeMap<Integer, Territorio> mapaTerr = jogo.getMapaIdTerritorios();
        for (Map.Entry<Integer, Territorio> entry : mapaTerr.entrySet()) {
            Territorio territorio = entry.getValue();
            if (territorio.id_jogador == this.id) {
                m_continentes.put(territorio.id_continente, jogo.getMapIdContinente().get(territorio.id_continente));

                if (m_idContinentesTerritorios.containsKey(territorio.id_continente)) {
                    TreeMap<Integer, Territorio> territorios = m_idContinentesTerritorios.get(territorio.id_continente);
                    territorios.put(territorio.id, territorio);
                } else {
                    TreeMap<Integer, Territorio> territorios = new TreeMap<>();
                    territorios.put(territorio.id, territorio);
                    m_idContinentesTerritorios.put(territorio.id_continente, territorios);

                }

                if (m_territoriosPorContinente.containsKey(territorio.id_continente)) {
                    Integer value = m_territoriosPorContinente.get(territorio.id_continente);
                    value += 1;
                } else {
                    m_territoriosPorContinente.put(territorio.id_continente, 1);
                }
            }
        }

        m_acabarTurno = false;
    }

    private Integer escolherDistribuicaoInicial(Jogo jogo) {

        Collection<Continente> conts = jogo.getMapIdContinente().values();
        List<Continente> list0 = new ArrayList<>(conts);
        Collections.sort(list0, m_continenteComparator);
        
        List<Continente> list = new ArrayList<>(m_continentes.values());
        Collections.sort(list, m_continenteComparator);

        List< Territorio> list2 = new ArrayList<>(m_idContinentesTerritorios.get(list.get(0).id).values());
        Collections.sort(list2, m_territorioComparator);

        return list2.get(0).id;
    }

}
