package Modelo;

import Controle.WarControl;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 */
public class CartaObjetivo implements Carta {
    private int id;
    private String desc;
    private String caminho;

    /**
     *
     * @param id_carta
     * @param descricao
     * @param caminho
     */
    public CartaObjetivo(int id_carta, String descricao, String caminho) {
        this.id = id_carta;
        this.desc = descricao;
        this.caminho = caminho;
    }

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return this.desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCaminho() {
        return this.caminho;
    }
    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }
    
    public boolean confereObjetivo()
    {
        int idHumano = 1; //Pelo que eu entendi, o id do Humano é sempre 1. Certo????
        //Continente X está salvo no índice (idContinente - 1). Certo????
        Continente americaSul = WarControl.me().getJogo().getMapIdContinente().get(1-1);
        Continente americaNorte = WarControl.me().getJogo().getMapIdContinente().get(2-1);
        Continente africa = WarControl.me().getJogo().getMapIdContinente().get(3-1);
        Continente asia = WarControl.me().getJogo().getMapIdContinente().get(4-1);
        Continente europa = WarControl.me().getJogo().getMapIdContinente().get(5-1);
        Continente oceania = WarControl.me().getJogo().getMapIdContinente().get(6-1);
        switch(id){
            case 1: //Conquistar na totalidade a EUROPA, a OCEANIA e mais um terceiro continente qualquer
                if(europa.completamenteConquistado(idHumano) && oceania.completamenteConquistado(idHumano)
                        &&(americaSul.completamenteConquistado(idHumano) || americaNorte.completamenteConquistado(idHumano)
                            || africa.completamenteConquistado(idHumano) || asia.completamenteConquistado(idHumano)))
                    return true;
                break;
                
            case 2: //Conquistar na totalidade a ÁSIA e a AMÉRICA DO SUL
                if(asia.completamenteConquistado(idHumano) && americaSul.completamenteConquistado(idHumano))
                    return true;
                break;
                
            case 3: //Conquistar na totalidade a EUROPA, a AMÉRICA DO SUL e mais um terceiro continente qualquer
                if(europa.completamenteConquistado(idHumano) && americaSul.completamenteConquistado(idHumano)
                        &&(oceania.completamenteConquistado(idHumano) || americaNorte.completamenteConquistado(idHumano)
                            || africa.completamenteConquistado(idHumano) || asia.completamenteConquistado(idHumano)))
                    return true;
                break;
                
            case 4: // Conquistar 18 territórios com no mínimo 2 exércitos em cada um.
                if(confereXTerritoriosComYPeças(idHumano, 18, 2))
                    return true;
                break;
                
            case 5: //Conquistar na totalidade a ÁSIA e a ÁFRICA
                if(asia.completamenteConquistado(idHumano) && africa.completamenteConquistado(idHumano))
                    return true;
                break;
                
            case 6: //Conquistar na totalidade a AMÉRICA DO NORTE e a ÁFRICA
                if(americaNorte.completamenteConquistado(idHumano) && africa.completamenteConquistado(idHumano))
                    return true;
                break;
                
            case 7: //Conquistar 24 TERRITÓRIOS à sua escolha
                if(confereXTerritoriosComYPeças(idHumano, 24, 1))
                    return true;
                break;
                
            case 8: //Conquistar na totalidade a AMÉRICA DO NORTE e a OCEANIA
                if(americaNorte.completamenteConquistado(idHumano) && oceania.completamenteConquistado(idHumano))
                    return true;
                break;
                

            case 9: //Destruir totalmente OS EXÉRCITOS AZUIS
                if(exercitoDestruido("blue", idHumano))
                    return true;
                break;
                
            case 10: //Destruir totalmente OS EXÉRCITOS AMARELOS
                 if(exercitoDestruido("orange", idHumano))
                    return true;
                break;
                
            case 11: //Destruir totalmente OS EXÉRCITOS VERMELHOS
                 if(exercitoDestruido("red", idHumano))
                    return true;
                break;
                
            case 12: //Destruir totalmente OS EXÉRCITOS PRETOS
                if(exercitoDestruido("brown", idHumano))
                    return true;
                break;
                
            case 13: //Destruir totalmente OS EXÉRCITOS BRANCOS
                if(exercitoDestruido("gray", idHumano))
                    return true;
                break;
                
            case 14: //Destruir totalmente OS EXÉRCITOS VERDES
                if(exercitoDestruido("green", idHumano))
                    return true;
                break;
                
            default:
                return false;
        }
        return false;
    }
    
    private boolean confereXTerritoriosComYPeças(int idJogador, int X, int Y) {
        int qtd = 0; //Quantidade de territórios com no mínimo 2 peças
        TreeMap<Integer, Continente> continentes = WarControl.me().getJogo().getMapIdContinente();
        for (Map.Entry<Integer, Continente> entry : continentes.entrySet()) { //Varre por Continente            
            Continente continente = entry.getValue();
            ArrayList<Territorio> territorios = continente.territorios;
            for (Territorio territorio : territorios) {
                //Varre os territórios do continente de índice i
                if (territorio.id_jogador == idJogador && territorio.exercitos >= Y) {
                    qtd++;
                }
            }
        }
        
        if(qtd >= X)
            return true;
        else
            return false;
    }

    private boolean exercitoDestruido(String _corDestruir, int idHumano) {
        TreeMap<Integer, Jogador> mapaJogadores = WarControl.me().getJogo().getMapIdJogador();
        Jogador jogador = mapaJogadores.get(idHumano);
        if(jogador.cor == _corDestruir) //Cor a destruir é do próprio jogador
            return confereXTerritoriosComYPeças(idHumano, 24, 1);
        
        Jogador jogDestruir = null;
        for(Map.Entry<Integer, Jogador> entry : mapaJogadores.entrySet())
        {
            Jogador itJog = entry.getValue();
            if(itJog.cor == _corDestruir)
            {
                jogDestruir = itJog;
                break;
            }
        }
        
        if(jogDestruir == null) //Cor a destruir não é de nenhum jogador
            return confereXTerritoriosComYPeças(idHumano, 24, 1);
        
        int idDestruidor = WarControl.me().getJogo().quemDestruiuJogador(jogDestruir.id);        
        
        if(jogador.id != idDestruidor) //Cor a destruir destruida por outro jogador
            return confereXTerritoriosComYPeças(idHumano, 24, 1);
        
        return true;
    }
}
