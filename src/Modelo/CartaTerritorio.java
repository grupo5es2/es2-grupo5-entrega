package Modelo;

/**
 * Cartas de territórios, para trocas e bônus.
 */
public class CartaTerritorio implements Carta {
    private int id;
    private String geometria;
    private Territorio territorio;    
    private String nome;
    private String simbolo;
    private String caminho;

    /**
     * Construtor padrão, com identificador
     * @param id_carta
     */
    public CartaTerritorio(int id_carta) {
        this.id = id_carta;
    }
    
    public CartaTerritorio(int id_carta, String nome, String simbolo, String caminho) {
        this.id = id_carta;
        this.nome = nome;
        this.simbolo = simbolo;
        this.caminho = caminho;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getGeometria() {
        return geometria;
    }
    public void setGeometria(String geometria) {
        this.geometria = geometria;
    }

    public Territorio getTerritorio() {
        return territorio;
    }
    public void setTerritorio(Territorio territorio) {
        this.territorio = territorio;
    }
    
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getSimbolo() {
        return simbolo;
    }
    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }
    
    public String getCaminho() {
        return caminho;
    }
    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }
}
