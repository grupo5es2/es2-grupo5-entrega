package Modelo;

import java.util.ArrayList;

/**
 *
 */
public class Continente {
    int id;    
    String nome;
    int bonus;
    ArrayList<Territorio> territorios;    

    /**
     * Construtor padrão, com identificador
     */
    public Continente(int _id, String _nome, int _bonus, ArrayList<Territorio> _terr) {
        this.id = _id;        
        this.nome = _nome;
        this.bonus = _bonus;
        this.territorios = _terr;        
    }
    
    //Se o Continente está conquistado por um jogador de id = idJogador, então TRUE.
    public boolean completamenteConquistado(int idJogador)
    {
        for (int i = 0; i < territorios.size(); i++) {
            if(idJogador != territorios.get(i).id_jogador)
                return false;
        }
        return true;
    }
}
