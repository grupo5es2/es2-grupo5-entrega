/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.Random;

abstract class RandomGen
{    
    public abstract int next(int lados);
}

class RandomGenDefImpl extends RandomGen {
    private Random r;
    
    public RandomGenDefImpl()
    {
        r = new Random();
    }
    
    @Override
    public int next(int lados) {
        return r.nextInt(lados) + 1;
    }    
}

/**
 *
 * @author alexandre
 */
public class Dado {
    
    public static int rolar(int dados, int lados)            
    {        
        RandomGen r = new RandomGenDefImpl();        
        int total = 0;
        for(int dado = 0; dado < dados; ++dado )
            total += r.next(lados);
        return total;        
    }

    public static ArrayList<Integer> rolarLista(int dados, int lados)
    {
        ArrayList<Integer> lista = new ArrayList<>();
        for(int dado = 0; dado < dados; ++dado)
            lista.add(rolar(1, lados));
        return lista;        
    }
}
