package Modelo;

/**
 *
 */
public class Humano extends Jogador {

    /**
     * Construtor padrão, com identificador
     * @param id_jogador
     */
    public Humano(int id_jogador) {
        super(id_jogador);
        nome = "Humano";
    }

    @Override
    boolean ehJogadorHumano() {
        return true;
    }
    
}
