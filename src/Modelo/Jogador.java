package Modelo;

import java.util.ArrayList;

/**
 *
 */
public abstract class Jogador {
    
    /**
     *
     */
    public int id;

    /**
     *
     */
    public String nome;

    /**
     *
     */
    public String cor;

    /**
     *
     */
    public CartaObjetivo objetivo;
    
    public boolean m_acabarTurno;    

    /**
     *
     */
    public ArrayList<CartaTerritorio> cartas;

    /**
     * Construtor padrão, com identificador
     * @param id_jogador
     */
    public Jogador(int id_jogador) {
        this.id = id_jogador;
        this.cartas = new ArrayList<>();
    }
    
    /**
     *
     */
    public void atacar(Jogo jogo) {
        //codigo de atacar
}
    
    /**
     *
     */
    public void remanejar(Jogo jogo) {
        //codigo de remanejar
    }
    
    /**
     *
     */
    public void trocarCartas(Jogo jogo) {
        //codigo de trocar cartas
    }

    abstract boolean ehJogadorHumano();
    
    public void setAcabouTurno(boolean val) {
        m_acabarTurno = val;
    }
    public boolean acabouTurno() {
        return m_acabarTurno;
    }
    
    public CartaObjetivo getObjetivo() {
        return this.objetivo;
    }
    
    public void setObjetivo(CartaObjetivo carta) {
        this.objetivo = carta;
    }
}
