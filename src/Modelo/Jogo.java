package Modelo;

import Controle.WarControl;
import Interface.TelaFimJogo;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 */
public class Jogo {

    public static Logger logger() {
        return Logger.getLogger(Jogo.class.getName());
    }

    private int m_numeroRodada;
    private int m_numeroTurno;
    private int nTroca;
    private ArrayList<Integer> listaDadosAtaque, listaDadosDefesa;

    final private String[] cores = {"blue",
        "brown",
        "gray",
        "green",
        "orange",
        "red"};
    
    private ArrayList<CartaTerritorio> m_cartasTerritoriosJogo = new ArrayList<>();
    private boolean m_atualPegaCarta;    
    private Random m_random;

    void realizarAtaque(int idAtaque, int idDefesa, int exercitos) {
        Territorio atacante = m_mapaIdTerritorios.get(idAtaque);
        Territorio defensor = m_mapaIdTerritorios.get(idDefesa);

        atribuirAtacante(idAtaque, exercitos);
        atribuirDefensor(idDefesa, Math.min(defensor.exercitos, 3));
        rolarDadosJogador(atacante.id_jogador);
        rolarDadosJogador(defensor.id_jogador);
        consolidarAtaque();
    }

    void realizarAtaque_Old(int idAtaque, int idDefesa, int exercitos) {
        Territorio atacante = m_mapaIdTerritorios.get(idAtaque);
        Territorio defensor = m_mapaIdTerritorios.get(idDefesa);

        int dadosAtaque = exercitos;
        int dadosDefesa = Math.min(defensor.exercitos, exercitos);

        int lados = 6;

        listaDadosAtaque = Dado.rolarLista(dadosAtaque, lados);
        Collections.sort(listaDadosAtaque);

        listaDadosDefesa = Dado.rolarLista(dadosDefesa, lados);
        Collections.sort(listaDadosDefesa);

        int ataquesVitoriosos = 0;
        for (int dado = 0; dado < dadosDefesa; ++dado) {
            int resultadoAtaque = listaDadosAtaque.get(dado);
            int resultadoDefesa = listaDadosDefesa.get(dado);
            if (resultadoAtaque > resultadoDefesa) {
                ++ataquesVitoriosos;
            }
        }

        defensor.exercitos -= ataquesVitoriosos;
        atacante.exercitos -= dadosDefesa - ataquesVitoriosos;

        if (defensor.exercitos < 1) {
            defensor.id_jogador = atacante.id_jogador;
            defensor.exercitos = dadosAtaque;
            atacante.exercitos -= dadosAtaque;
        }
    }

    private int m_idTerritorioAtaque, m_dadosAtaque, m_idTerritorioDefesa, m_dadosDefesa;

    public void atribuirAtacante(int idTerritorio, int dados) {
        m_idTerritorioAtaque = idTerritorio;
        m_dadosAtaque = dados;
    }

    public void atribuirDefensor(int idTerritorio, int dados) {
        m_idTerritorioDefesa = idTerritorio;
        m_dadosDefesa = dados;
    }

    public ArrayList<Integer> rolarDadosJogador(int id) {
        if (m_mapaIdTerritorios.get(m_idTerritorioAtaque).id_jogador == id) {
            listaDadosAtaque = Dado.rolarLista(m_dadosAtaque, 6);
            Collections.sort(listaDadosAtaque, Collections.reverseOrder());
            return listaDadosAtaque;
        } else if (m_mapaIdTerritorios.get(m_idTerritorioDefesa).id_jogador == id) {
            listaDadosDefesa = Dado.rolarLista(m_dadosDefesa, 6);
            Collections.sort(listaDadosDefesa, Collections.reverseOrder());
            return listaDadosDefesa;
        }

        return new ArrayList<>();
    }

    public ArrayList<Integer> consolidarAtaque() {
        Territorio atacante = m_mapaIdTerritorios.get(m_idTerritorioAtaque);
        Territorio defensor = m_mapaIdTerritorios.get(m_idTerritorioDefesa);

        logger().log(Level.SEVERE, "Ataque! {0}({4}) em {1}({6}) contra {2}({5}) em {3}({7})", new Object[]{getJogadorAtual().nome, getMapaIdTerritorios().get(m_idTerritorioAtaque).nome, getMapIdJogador().get(defensor.id_jogador).nome, getMapaIdTerritorios().get(m_idTerritorioDefesa).nome, listaDadosAtaque.size(), listaDadosDefesa.size(), atacante.exercitos, defensor.exercitos});

        ArrayList<Integer> resultado = new ArrayList<>();

        int dadosComparar = Math.min(listaDadosDefesa.size(), listaDadosAtaque.size());
        int ataquesVitoriosos = 0;
        for (int dado = 0; dado < dadosComparar; ++dado) {
            int resultadoAtaque = listaDadosAtaque.get(dado);
            int resultadoDefesa = listaDadosDefesa.get(dado);
            if (resultadoAtaque > resultadoDefesa) {
                resultado.add(1);
                ++ataquesVitoriosos;
            } else {
                resultado.add(-1);
            }
        }

        int defesasVitoriosas = dadosComparar - ataquesVitoriosos;

        defensor.exercitos -= ataquesVitoriosos;
        atacante.exercitos -= defesasVitoriosas;
        
        String msg = MessageFormat.format("DadosAtaque[{0}] x [{1}]DadosDefesa\n", new Object[]{listaDadosAtaque, listaDadosDefesa});
        msg = msg.concat(MessageFormat.format( "Ataque! Resultado Ataque {0} x Defesa {1}", new Object[]{ataquesVitoriosos, defesasVitoriosas}));        
        logger().log(Level.SEVERE, msg);

        if (defensor.exercitos < 1) //Território conquistado
        {
            m_mapaJogadorTerritorios.get(atacante.id_jogador).add(defensor);
            m_mapaJogadorTerritorios.get(defensor.id_jogador).remove(defensor);
            
            if(m_mapaJogadorTerritorios.get(defensor.id_jogador).isEmpty()) // Destruiu jogador
                m_mapaJogadoresDestruidos.put(defensor.id_jogador, atacante.id_jogador);

            defensor.id_jogador = atacante.id_jogador;
            defensor.exercitos = m_dadosAtaque - defesasVitoriosas;
            atacante.exercitos -= (m_dadosAtaque - defesasVitoriosas);
            setPegaCarta(true);
            logger().log(Level.SEVERE, "Ataque! Territorio {0} tomado!", new Object[]{defensor.nome});

        }

        return resultado;
    }

    private int getNumJogadores() {
        return m_arrOrdemJogadores.size();
    }

    public int getRestanteDistribuicaoInicial(int jogador) {
        if(!m_mapaJogadorDistribuicoesRestantes.containsKey(jogador))
            return 0;
        return m_mapaJogadorDistribuicoesRestantes.get(jogador);
    }
    
    public int getExercitosAdicionais(int idJogador)
    {
        Jogador jogador = m_mapaIdJogadores.get(idJogador);
        ArrayList<Territorio> territorios = m_mapaJogadorTerritorios.get(jogador.id);
        return (int) Math.floor(territorios.size() / 2);
    }

    public int getDistribuicaoRodada(int jogadorAtualIndice) {
        Jogador jogador = m_mapaIdJogadores.get(jogadorAtualIndice);
        
        int distribuicaoRodada = getExercitosAdicionais(jogador.id);

        int bonusContinentes = getBonusContinentes(jogador.id);

        int bonusTrocas = getBonusTrocas(jogador.id);

        return distribuicaoRodada + bonusContinentes + bonusTrocas;
    }
    
    public int calculaTroca()
    {
        int qtd = WarControl.me().getTelaMapa().getNumTrocas();
        
        if(qtd == 1)
            return 4;
        else if(qtd == 2)
            return 6;
        else if(qtd == 3)
            return 8;
        else if(qtd == 4)
            return 10;
        else if(qtd == 5)
            return 12;
        else if(qtd == 6)
            return 15;
        else
        {
            int exercitos = 25;
            int aux = 7;
            while(true){
                
                if(aux == qtd)
                    return exercitos;
                else
                {
                    aux++;
                    exercitos += 5;
                }
            }
        }
        
        //return qtd;
    }

    private int getBonusContinentes(int id) {
        Jogador jogador = m_mapaIdJogadores.get(id);

        int totalBonus = 0;
        for (Map.Entry<Integer, Continente> entry : m_mapaIdContinente.entrySet()) {
            Continente continente = entry.getValue();
            if (continente.completamenteConquistado(jogador.id)) {
                totalBonus += continente.bonus;
            }
        }

        return totalBonus;
    }

    private int getBonusTrocas(int id) {
        if(!m_mapaJogadorTrocas.containsKey(id))
            return 0;
        
        return m_mapaJogadorTrocas.get(id);
    }

    public boolean remanejar(int _idOrigem, int _idDestino) {
        Territorio origem = m_mapaIdTerritorios.get(_idOrigem);
        Territorio destino = m_mapaIdTerritorios.get(_idDestino);
        if(origem.id_jogador == destino.id_jogador)
        {
            if(origem.vizinhos.containsKey(destino.id) && destino.vizinhos.containsKey(origem.id))
            {                
                origem.exercitos -= 1;
                destino.exercitos += 1;
                logger().log(Level.SEVERE, String.format("Remanejamento! %1$s para %2$s", origem.nome, destino.nome), "");
                return true;
            }
            else
                logger().log(Level.SEVERE, String.format("Falha ao remanejar! Erro de vizinhança! %1$s para %2$s", origem.nome, destino.nome), "");
        }
        else
            logger().log(Level.SEVERE, String.format("Falha ao remanejar! Territorios nao sao do mesmo jogador! %1$s para %2$s", origem.nome, destino.nome), "");
        
        return false;                            
    }

    public CartaTerritorio sortearCartaTerritorio() {        
        if(m_cartasTerritoriosJogo.isEmpty())
        {
            Random rnd = getRandom();
            m_cartasTerritoriosJogo = new ArrayList<>(m_arrCartaTerritorios);
            Collections.shuffle(m_cartasTerritoriosJogo, rnd);//Sorteia Cartas de Territorio.
        }
        
        CartaTerritorio cartaSorteada = m_cartasTerritoriosJogo.remove(0);//Remove carta do baralho sorteado.        
        return cartaSorteada;
    }

    public void setPegaCarta(boolean b) {
        m_atualPegaCarta = b;
    }
    
    public boolean pegaCarta() {
        return m_atualPegaCarta;
    }

    int quemDestruiuJogador(int id) {
        if(!m_mapaJogadoresDestruidos.containsKey(id))
            return -1;
        return m_mapaJogadoresDestruidos.get(id);
    }

    private Random getRandom() {
        if(m_random == null)
            m_random = new Random(System.currentTimeMillis());
        return m_random;
    }
    
    public boolean tentarTrocaCartasIguais(int idJogador, ArrayList<CartaTerritorio> cartas, int numMinCartas)
    {
        if (cartas.size() >= numMinCartas) {
            Jogador jogador = m_mapaIdJogadores.get(idJogador);
            for (int i = 0; i < numMinCartas; ++i) {
                jogador.cartas.remove(cartas.get(i));
            }            
            m_mapaJogadorTrocas.put(jogador.id, calculaTroca());
            m_mapaJogadorDistribuicoesRestantes.put(jogador.id, getDistribuicaoRodada(jogador.id));
            
            return true;
        }
        
        return false;        
    }
    
    public boolean tentarTrocaCartasDistintas(int idJogador, ArrayList<CartaTerritorio> cartasQ,
            ArrayList<CartaTerritorio> cartasC, ArrayList<CartaTerritorio> cartasT, int numMinCartas)
    {
        int contaNaoVazio = 0;
        if(cartasQ.isEmpty()) ++contaNaoVazio;
        if(cartasC.isEmpty()) ++contaNaoVazio;
        if(cartasT.isEmpty()) ++contaNaoVazio;
        
        if( contaNaoVazio >= numMinCartas )
        {            
            int total = cartasQ.size() + cartasC.size() + cartasT.size();
            if(total >= numMinCartas)
            {
                Jogador jogador = m_mapaIdJogadores.get(idJogador);
                
                if(!cartasQ.isEmpty())
                    jogador.cartas.remove(cartasQ.get(0));
                
                if (!cartasC.isEmpty()) {
                    jogador.cartas.remove(cartasC.get(0));
                }

                if (!cartasT.isEmpty()) {
                    jogador.cartas.remove(cartasT.get(0));
                }             
                
                m_mapaJogadorTrocas.put(jogador.id, calculaTroca());
                m_mapaJogadorDistribuicoesRestantes.put(jogador.id, getDistribuicaoRodada(jogador.id));
                
                return true;
            }                
        }
        return false;
    }

    public boolean tentarFazerTroca() {
        Jogador jogador = getJogadorAtual();
        
        ArrayList<CartaTerritorio> quadrados, circulos, triangulos, coringas;
        quadrados = new ArrayList<>();
        circulos =  new ArrayList<>();
        triangulos = new ArrayList<>();
        coringas = new ArrayList<>();
        
        for(CartaTerritorio carta : jogador.cartas)
        {
            switch(carta.getSimbolo())
            {
                case "Q": quadrados.add(carta); break;
                case "C": circulos.add(carta); break;
                case "T": triangulos.add(carta); break;
                case "?": coringas.add(carta); break;
            }                
        }
        
        if(tentarTrocaCartasIguais(jogador.id, quadrados, 3))
            return true;
        
        if(tentarTrocaCartasIguais(jogador.id, circulos, 3))
            return true;
        
        if(tentarTrocaCartasIguais(jogador.id, triangulos, 3))
            return true;
        
        if(tentarTrocaCartasDistintas(jogador.id, quadrados, circulos, triangulos, 3))
            return true;

        if (!coringas.isEmpty())
        {
            boolean trocaComCoringa = false;
            
            if (tentarTrocaCartasIguais(jogador.id, quadrados, 2)) {
                trocaComCoringa = true;
            }
            else if (tentarTrocaCartasIguais(jogador.id, circulos, 2)) {
                trocaComCoringa = true;
            }
            else if (tentarTrocaCartasIguais(jogador.id, triangulos, 2)) {
                trocaComCoringa = true;
            }
            else if (tentarTrocaCartasDistintas(jogador.id, quadrados, circulos, triangulos, 2)) {
                trocaComCoringa = true;
            }
            
            if(trocaComCoringa)
            {
                jogador.cartas.remove(coringas.get(0));            
                return true;
            }
        }

        return false;        
    }

    public void efetuarTroca(ArrayList<CartaTerritorio> listaCartasTrocas) {
        Jogador jogador = getJogadorAtual();
        m_mapaJogadorTrocas.put(jogador.id, calculaTroca());
        m_mapaJogadorDistribuicoesRestantes.put(jogador.id, getDistribuicaoRodada(jogador.id));
        
        for (int i = 0; i < listaCartasTrocas.size(); i++)
            jogador.cartas.remove(listaCartasTrocas.get(i));        
    }

    public enum ESTADO {

        INICIO,
        DISTRIBUICAO_INICIAL,
        DISTRIBUICAO_RODADA,
        ATAQUE,
        DISTRIBUICAO,
        TROCA
    }

    public ESTADO m_estado;
    public CartaObjetivo m_objetivoHumano;
    //private ArrayList<CartaTerritorio> m_listaCartaTerritoriosHumano;
    private Integer m_indiceAtual;

    //private int m_distribuicaoInicial = 0;
    private String m_strMapFile;
    private TreeMap<Integer, Jogador> m_mapaIdJogadores;
    private TreeMap<Integer, Territorio> m_mapaIdTerritorios;
    private TreeMap<Integer, Continente> m_mapaIdContinente;
    private ArrayList<CartaObjetivo> m_arrObjetivos;
    private ArrayList<CartaTerritorio> m_arrCartaTerritorios;
    private ArrayList<Integer> m_arrOrdemJogadores;

    private TreeMap<Integer, Integer> m_mapaJogadoresDestruidos;
    private TreeMap<Integer, ArrayList<Territorio>> m_mapaJogadorTerritorios;
    private TreeMap<Integer, Integer> m_mapaJogadorDistribuicoesRestantes;
    private TreeMap<Integer, Integer> m_mapaJogadorTrocas;

    /**
     *
     */
    public Jogo() {
        carregarConfiguracoes();

        m_indiceAtual = 0;
        m_estado = ESTADO.INICIO;
        nTroca = 0;
    }

    public int getNTroca() {
        return this.nTroca;
    }

    /**
     *
     * @return
     */
    public String getMapaFileName() {
        return m_strMapFile;
    }
    
     public ArrayList<Territorio> getTerritoriosJogador(int idJogador) {
        return m_mapaJogadorTerritorios.get(idJogador);
    }

    public ArrayList<CartaTerritorio> getListaCartasJogador() {
        //m_listaCartaTerritoriosHumano = m_mapaIdJogadores.get(1).cartas;
        //return m_listaCartaTerritoriosHumano;
        return m_mapaIdJogadores.get(1).cartas;
    }

    public void setListaCartasJogador(ArrayList<CartaTerritorio> lista) {
        m_mapaIdJogadores.get(1).cartas = lista;
    }

    private void carregarConfiguracoes() {
        /*
         <root>
         <territorio id=\"\" nome=\"\" centro=\"\" vizinhos=\"\" />            
         <continente id=\"\" nome=\"\" territorios=\"\" />
         <objetivo id=\"\" />
         </root>
         */
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(false);

        DocumentBuilder db;
        Document document;

        try {
            db = dbf.newDocumentBuilder();
            document = db.parse(new File("resource/mapa_default.xml"));

            m_mapaIdTerritorios = carregarMapa(document);

            m_arrCartaTerritorios = carregarCartas(document);
            m_arrObjetivos = carregarObjetivos(document);

            m_mapaIdContinente = carregarContinentes(document);

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            logger().log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param num_jogadores
     * @throws Exception
     */
    public void iniciaJogo(int num_jogadores) throws Exception {

        if (num_jogadores > 6 || num_jogadores < 3) {
            throw new Exception("");
        }

        m_mapaIdJogadores = new TreeMap<>();
        m_arrOrdemJogadores = new ArrayList<>();
        m_mapaJogadorTerritorios = new TreeMap<>();
        m_mapaJogadoresDestruidos = new TreeMap<>();
        m_mapaJogadorTrocas = m_mapaJogadorDistribuicoesRestantes = new TreeMap<>();
        //m_listaCartaTerritoriosHumano = new ArrayList<>();

        //Cria jogadores
        int id_jogador = 1;
        m_arrOrdemJogadores.add(id_jogador);
        m_mapaIdJogadores.put(id_jogador, new Humano(id_jogador)); //Humano
        
        Random rnd = getRandom();
        
        ArrayList<CartaObjetivo> cartasObjetivos = new ArrayList<>(m_arrObjetivos);
        Collections.shuffle(cartasObjetivos, rnd);//Sorteia objetivo
        
        m_objetivoHumano = cartasObjetivos.remove(0); //Remove objetivo sorteado (do Humano)
        logger().log(Level.SEVERE, Integer.toString(m_objetivoHumano.getId()));

        while (m_mapaIdJogadores.size() != num_jogadores) {
            id_jogador++;
            m_arrOrdemJogadores.add(id_jogador);
            m_mapaIdJogadores.put(id_jogador, new Bot(id_jogador)); //Bot

            //Designa objetivos para os bots
            CartaObjetivo m_objetivoBot = cartasObjetivos.remove(0);
            m_mapaIdJogadores.get(id_jogador).setObjetivo(m_objetivoBot);            
        }

        //Ordem aleatória de jogadores        
        Collections.shuffle(m_arrOrdemJogadores, rnd);

        //Atribuir cores
        ArrayList<String> arrCores = new ArrayList<String>(Arrays.asList(cores));
        Collections.shuffle(arrCores, rnd);
        for (int i = 0; i < getNumJogadores(); ++i) {
            id_jogador = m_arrOrdemJogadores.get(i);
            Jogador jogador = m_mapaIdJogadores.get(id_jogador);
            jogador.cor = arrCores.get(i);
        }

        //Distribuir territorios aos jogadores
        ArrayList<Territorio> listaTeritorios = new ArrayList<>(Arrays.asList(m_mapaIdTerritorios.values().toArray(new Territorio[]{})));
        Collections.shuffle(listaTeritorios, rnd);

        for (int i = 0; i < listaTeritorios.size(); ++i) {
            Territorio territorio = listaTeritorios.get(i);

            id_jogador = m_arrOrdemJogadores.get(i % getNumJogadores());

            Jogador jogador = m_mapaIdJogadores.get(id_jogador);

            territorio.id_jogador = jogador.id;
            territorio.exercitos = 1;

            if (m_mapaJogadorTerritorios.containsKey(id_jogador)) {
                m_mapaJogadorTerritorios.get(id_jogador).add(territorio);
            } else {
                ArrayList<Territorio> territoriosJogador = new ArrayList<>();
                territoriosJogador.add(territorio);
                m_mapaJogadorTerritorios.put(id_jogador, territoriosJogador);
            }
        }

        //Atribuir territorios iniciais;
        for (int i = 0; i < getNumJogadores(); ++i) {
            id_jogador = m_arrOrdemJogadores.get(i);
            Jogador jogador = m_mapaIdJogadores.get(id_jogador);
            m_mapaJogadorDistribuicoesRestantes.put(id_jogador, getDistribuicaoRodada(id_jogador));
        }

        //m_distribuicaoInicial = m_mapaIdTerritorios.size() / 2;
        m_estado = ESTADO.DISTRIBUICAO_INICIAL;
        m_numeroTurno = m_numeroRodada = 0;
        
        //codigo de iniciar jogo
    }

    private TreeMap<Integer, Territorio> carregarMapa(Document xml_document) {
        TreeMap<Integer, Territorio> novo_mapa;
        novo_mapa = new TreeMap<>();

        XPath xPath = XPathFactory.newInstance().newXPath();

        NodeList lista_territorio;
        try {
            m_strMapFile = (String) xPath.compile("/root/imagem/@arquivo").evaluate(xml_document, XPathConstants.STRING);

            lista_territorio = (NodeList) xPath.compile("/root//territorio").evaluate(xml_document, XPathConstants.NODESET);
            int num_territorios = lista_territorio.getLength();
            for (int i = 0; i < num_territorios; ++i) {
                Node no_territorio = lista_territorio.item(i);

                Integer id_num = ((Double) xPath.compile("@id").evaluate(no_territorio, XPathConstants.NUMBER)).intValue();

                String nome = (String) xPath.compile("@nome").evaluate(no_territorio, XPathConstants.STRING);

                String posicao_XY[] = ((String) xPath.compile("@centro").evaluate(no_territorio, XPathConstants.STRING)).split(",");
                double pos_X = Double.parseDouble(posicao_XY[0]);
                double pos_Y = Double.parseDouble(posicao_XY[1]);

                Territorio territorio = new Territorio(id_num, nome, pos_X, pos_Y);
                novo_mapa.put(id_num, territorio);
            }

            for (int i = 0; i < num_territorios; ++i) {
                Node no_territorio = lista_territorio.item(i);
                Integer id_num = ((Double) xPath.compile("@id").evaluate(no_territorio, XPathConstants.NUMBER)).intValue();
                String vizinhos[] = ((String) xPath.compile("@vizinhos").evaluate(no_territorio, XPathConstants.STRING)).split(",");

                Territorio territorio = novo_mapa.get(id_num);

                for (int j = 0; j < vizinhos.length; ++j) {
                    Integer id_vizinho = Integer.parseInt(vizinhos[j]);
                    if (novo_mapa.containsKey(id_vizinho)) {
                        territorio.vizinhos.put(id_vizinho, novo_mapa.get(id_vizinho));
                    }
                }
            }

        } catch (XPathExpressionException ex) {
            logger().log(Level.SEVERE, null, ex);
        }

        return novo_mapa;
    }

    private ArrayList<CartaTerritorio> carregarCartas(Document xml_document) {
        ArrayList<CartaTerritorio> novas_cartas;
        novas_cartas = new ArrayList<>();

        XPath xPath = XPathFactory.newInstance().newXPath();

        NodeList lista_cartas;
        try {            
            lista_cartas = (NodeList) xPath.compile("/root//territorio").evaluate(xml_document, XPathConstants.NODESET);
            int num_cartas = lista_cartas.getLength();
            for (int i = 0; i < num_cartas; ++i) {
                Node no_territorio = lista_cartas.item(i);

                Integer id_num = ((Double) xPath.compile("@id").evaluate(no_territorio, XPathConstants.NUMBER)).intValue();
                String nome = (String) xPath.compile("@nome").evaluate(no_territorio, XPathConstants.STRING);
                String simbolo = (String) xPath.compile("@simbolo").evaluate(no_territorio, XPathConstants.STRING);
                String caminho = (String) xPath.compile("@caminho").evaluate(no_territorio, XPathConstants.STRING);

                CartaTerritorio territorio = new CartaTerritorio(id_num, nome, simbolo, caminho);
                novas_cartas.add(territorio);
            }
            
            CartaTerritorio territorio = new CartaTerritorio(++num_cartas, "Coringa", "?", "Territorios\\Coringa1");
            novas_cartas.add(territorio);
                
            territorio = new CartaTerritorio(++num_cartas, "Coringa", "?", "Territorios\\Coringa2");
            novas_cartas.add(territorio);
            
        } catch (XPathExpressionException ex) {
            logger().log(Level.SEVERE, null, ex);
        }
        return novas_cartas;
    }

    private ArrayList<CartaObjetivo> carregarObjetivos(Document xml_document) {
        ArrayList<CartaObjetivo> novos_objetivos;
        novos_objetivos = new ArrayList<>();

        XPath xPath = XPathFactory.newInstance().newXPath();

        NodeList lista_objetivo;
        try {
            lista_objetivo = (NodeList) xPath.compile("/root//objetivo").evaluate(xml_document, XPathConstants.NODESET);
            int num_objetivo = lista_objetivo.getLength();
            for (int i = 0; i < num_objetivo; ++i) {
                Node no_territorio = lista_objetivo.item(i);

                Integer id_num = ((Double) xPath.compile("@id").evaluate(no_territorio, XPathConstants.NUMBER)).intValue();
                String desc = (String) xPath.compile("@desc").evaluate(no_territorio, XPathConstants.STRING);
                String caminho = (String) xPath.compile("@caminho").evaluate(no_territorio, XPathConstants.STRING);

                CartaObjetivo objetivo = new CartaObjetivo(id_num, desc, caminho);
                novos_objetivos.add(objetivo);
            }
        } catch (XPathExpressionException ex) {
            logger().log(Level.SEVERE, null, ex);
        }

        return novos_objetivos;
    }

    private TreeMap<Integer, Continente> carregarContinentes(Document xml_document) {
        TreeMap<Integer, Continente> novo_mapa;
        novo_mapa = new TreeMap<>();

        XPath xPath = XPathFactory.newInstance().newXPath();

        NodeList lista_continente;
        try {

            lista_continente = (NodeList) xPath.compile("/root//continente").evaluate(xml_document, XPathConstants.NODESET);
            int num_continente = lista_continente.getLength();
            for (int i = 0; i < num_continente; ++i) {
                Node no_territorio = lista_continente.item(i);

                Integer id_cont = ((Double) xPath.compile("@id").evaluate(no_territorio, XPathConstants.NUMBER)).intValue();

                Integer bonus = ((Double) xPath.compile("@bonus").evaluate(no_territorio, XPathConstants.NUMBER)).intValue();

                String nome = (String) xPath.compile("@nome").evaluate(no_territorio, XPathConstants.STRING);

                String[] ids_terr = ((String) xPath.compile("@territorios").evaluate(no_territorio, XPathConstants.STRING)).split(",");

                ArrayList<Territorio> lista_terr = new ArrayList<>();
                for (int ii = 0; ii < ids_terr.length; ++ii) {
                    int id_terr = Integer.parseInt(ids_terr[ii]);
                    Territorio terr = m_mapaIdTerritorios.get(id_terr);
                    terr.id_continente = id_cont;
                    lista_terr.add(terr);
                }

                Continente continente = new Continente(id_cont, nome, bonus, lista_terr);
                novo_mapa.put(id_cont, continente);
            }
        } catch (XPathExpressionException ex) {
            logger().log(Level.SEVERE, null, ex);
        }

        return novo_mapa;
    }

    /**
     *
     * @return
     */
    public TreeMap<Integer, Territorio> getMapaIdTerritorios() {
        return m_mapaIdTerritorios;
    }

    /**
     *
     * @return
     */
    public TreeMap<Integer, Jogador> getMapIdJogador() {
        return m_mapaIdJogadores;
    }

    /**
     *
     * @return
     */
    public TreeMap<Integer, Continente> getMapIdContinente() {
        return m_mapaIdContinente;
    }

    public int getJogadorAtualIndice() {
        return m_arrOrdemJogadores.get(m_indiceAtual);
    }

    public Jogador getJogadorAtual() {
        return m_mapaIdJogadores.get(getJogadorAtualIndice());
    }

    public ESTADO getEstado() {
        return m_estado;
    }

    public void setEstado(ESTADO estado) {
        this.m_estado = estado;
    }

    public void proximoJogador() {
        m_indiceAtual = (m_indiceAtual + 1) % getNumJogadores();
        m_mapaIdJogadores.get(getJogadorAtualIndice()).setAcabouTurno(false);

    }

    public int jogadorProximo() {
        int prox = (m_indiceAtual + 1) % getNumJogadores();
        return m_arrOrdemJogadores.get(prox);
    }

    private void verificarAcabouDistribuicaoInicial() {

        if (m_mapaJogadorDistribuicoesRestantes.isEmpty()) {
            m_estado = ESTADO.ATAQUE;
        }
    }

    public void fimTurno() {
        Jogador jogador = m_mapaIdJogadores.get(getJogadorAtualIndice());

        switch (m_estado) {            

            case DISTRIBUICAO_INICIAL: {
                verificarAcabouDistribuicaoInicial();
                break;
            }
            
            case DISTRIBUICAO_RODADA: {
                
                if(!m_mapaJogadorDistribuicoesRestantes.containsKey(jogador.id))
                {
                    m_mapaJogadorTrocas.remove(jogador.id);
                    m_estado = ESTADO.ATAQUE;
                }
                
                break;
            }

            case TROCA: {
                //Mostrar cartas da mão
                //Permitir selecionar até 3 cartas
                //Se as 2 primeiras forem iguais, limitar a 3ª para ser igual tbm
                //Senão, a 3ª carta deve ser NECESSARIAMENTE diferente das 2 primeiras

                    //Se o jogador confirmar a troca:
                //Inserir 1 peça em cada território das cartas trocadas (se o jogador tiver)
                //Distribuir X peças (X = nº de peças referentes à troca)
                //Devolver as 3 cartas para a mesa
                //Senão, cancelar troca
                break;
            }

            default: {

                break;
            }
        }

        if (WarControl.me().getObjetivoHumano().confereObjetivo()) {
            TelaFimJogo telaFim = new TelaFimJogo(WarControl.me().getTelaMapa().getJanela(), WarControl.me().getJogo().getJogadorAtual());
            telaFim.executar();
        }
        
        if (jogador.acabouTurno()) {
            //Se jogador conquistou algum território, receber carta.
            if (pegaCarta()) {

                CartaTerritorio cartaSorteada = sortearCartaTerritorio();
                getJogadorAtual().cartas.add(cartaSorteada); //Jogador atual pega a carta
                setPegaCarta(false);                
            }
            
            if(m_estado == ESTADO.DISTRIBUICAO)
                m_estado = ESTADO.ATAQUE;

            if(m_estado != ESTADO.DISTRIBUICAO_INICIAL)
            {
                if(acabouRodada())
                    ++m_numeroRodada;
                ++m_numeroTurno;
            }
           
            proximoJogador();
                
            if((m_numeroTurno) > getNumJogadores())
            {
                m_estado = ESTADO.DISTRIBUICAO_RODADA;
                int idJogador = getJogadorAtualIndice();
                m_mapaJogadorDistribuicoesRestantes.put(idJogador, getDistribuicaoRodada(idJogador) );
            }
                
        }
    }
    
    public boolean acabouRodada()
    {
        return m_indiceAtual == (m_arrOrdemJogadores.size()-1);
    }
    
    public boolean distribuirEmTerritorio(Integer key) {
        Territorio territorio = m_mapaIdTerritorios.get(key);
        Jogador jogador = m_mapaIdJogadores.get(getJogadorAtualIndice());
        if(territorio.id_jogador == jogador.id)
        {
            if(m_mapaJogadorDistribuicoesRestantes.containsKey(jogador.id))                
            {
                Integer restante = m_mapaJogadorDistribuicoesRestantes.get(jogador.id);
            
                if(restante > 0)
                {                
                    --restante;
                    if(restante > 0)
                        m_mapaJogadorDistribuicoesRestantes.put(jogador.id, restante);
                    else if(restante == 0)
                        m_mapaJogadorDistribuicoesRestantes.remove(jogador.id);                

                    ++territorio.exercitos;

                    logger().log(Level.SEVERE, String.format("Jogador #####%1$s##### incrementou territorio: %2$s restante: %3$d", jogador.nome, territorio.nome, restante), "");

                    return true;
                }                
            }
            
            logger().log(Level.SEVERE, String.format("ERRO! Jogador %1$s tentou incrementar territorio: %2$s sem ter mais distribuicoes restantes", jogador.nome, territorio.nome), "");
        }
        else
            logger().log(Level.SEVERE, String.format("ERRO! Jogador %1$s tentou incrementar territorio: %2$s que não lhe pertence", jogador.nome, territorio.nome), "");
        
        return false;
    }

    public boolean ehJogadorHumano() {
        Jogador jogador = m_mapaIdJogadores.get(getJogadorAtualIndice());
        return jogador.ehJogadorHumano();
    }

    public void rodarIA() {
        Jogador jogador = getJogadorAtual();
        if (jogador instanceof Bot) {
            Bot bot = (Bot) jogador;

            logger().log(Level.SEVERE, "Em rodarIA() para #####{0}##### - Estado: {1}", new Object[]{bot.nome, m_estado.toString()}) ;

            bot.acao(m_estado, this);
        }
    }
}
