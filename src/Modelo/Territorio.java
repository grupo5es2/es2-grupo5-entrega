package Modelo;

import java.util.HashMap;

/**
 *
 */
public class Territorio {
    
    /**
     *
     */
    public int id;

    /**
     *
     */
    public String nome;    

    /**
     *
     */
    public HashMap<Integer, Territorio> vizinhos;
    
    /**
     *
     */
    public int exercitos;

    /**
     *
     */
    public int id_jogador;
    
     /**
     *
     */
    public int id_continente;

    /**
     *
     */
    public double pos_X;

    /**
     *
     */
    public double pos_Y;
    
    /**
     *
     * @param id
     * @param nome
     * @param pos_X
     * @param pos_Y
     */
    public Territorio(int id, String nome, double pos_X, double pos_Y) {
        this.id = id;
        this.nome = nome;
        this.pos_X = pos_X;
        this.pos_Y = pos_Y;
        this.vizinhos = new HashMap<>();
        
        this.id_jogador = 0;
        this.exercitos = 0;
    }
    
}
