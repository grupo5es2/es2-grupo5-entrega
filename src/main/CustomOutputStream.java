package main;
 
import java.io.IOException;
import java.io.OutputStream;
 
/**
 * This class extends from OutputStream to redirect output to a JTextArrea
 * @author www.codejava.net
 *
 */
public class CustomOutputStream extends OutputStream {
    private TraceWindow textArea;
    private String msg;
    
    public static String newline = System.getProperty("line.separator");
     
    public CustomOutputStream(TraceWindow textArea) {
        this.msg = new String();
        this.textArea = textArea;
    }

    //*
    @Override
    public void write(int b) throws IOException {
        // redirects data to the text area
        char cb = (char)b;
        switch(cb)
        {
            default:
                this.msg += String.valueOf((char)b); break;
            case '\0': case '\r': case '\n':
                if(this.msg.length() > 0)
                {
                    textArea.append(java.util.logging.Level.SEVERE.intValue(), "std", this.msg);
                    this.msg = "";
                }
        }
    }
    //*/
}