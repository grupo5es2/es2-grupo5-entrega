package main;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
 
public class TraceWindow
{
    JFrame window;
    JTextPane console;
    String separator;
 
    public TraceWindow(){
        System.setProperty( "java.awt.headless", "false" );
        separator = System.getProperty("line.separator");
    }
    
    private void appendToPane(JTextPane tp, String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);                
        
        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);        
        tp.setCharacterAttributes(aset, true);
        tp.replaceSelection(msg);
        
        /*
        StyledDocument sd = tp.getStyledDocument();
        sd.setCharacterAttributes(len, len + msg.length(), aset, true);
        */
    }
 
    public void activate() {
        window = new JFrame( "Trace window" );
        console = new JTextPane();
        String ct = console.getContentType();        
        console.setFont( new Font("Courier New", Font.PLAIN, 18) );        
        JScrollPane scroll = new JScrollPane( console );
        window.getContentPane().add(scroll);        
        window.setSize( 600, 600 );
        window.setLocation( 200, 200 );
        window.setVisible( true );
    }
 
    public void terminate() {
        window.dispose();
    }
    
    public void append( int level, String logger, String msg){
        append(level, logger, msg, Color.red);
    }
 
    public void append( int level, String logger, String msg, Color c ){
        appendToPane(console, msg, c);
        appendToPane(console, "\r\n", c);
        //console.setCaretPosition( console.getText().length() );
    }
}