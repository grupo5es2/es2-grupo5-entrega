package main;

import Controle.WarControl;
import Modelo.Jogo;
import Interface.TelaMenu;
import java.awt.Color;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

//////////////////////////////

//////////////////////////////

/**
 *

 */
public class main {
    
    /**
     *
     * @param args
     */
    public static void main(String[] args)  
    {
        
        TraceWindow trace = new TraceWindow();
        trace.activate();
        
        PrintStream printStream = new PrintStream(new CustomOutputStream(trace));
        System.setOut(printStream);
        System.setErr(printStream);
        Logger.getAnonymousLogger().getParent().setLevel(Level.SEVERE);
        
        Logger.getLogger(main.class.getName()).log(Level.SEVERE, "Saida logger", "");
        trace.append(Level.SEVERE.intValue(), null, "Saida direta na janela", Color.BLUE);
               
        WarControl.me().setJogo(new Jogo());
        WarControl.me().setTelaInicial(new TelaMenu());        
        WarControl.me().start();
    }  
}
